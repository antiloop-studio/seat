/*
* UniOSC
* Copyright © 2014-2015 Stefan Schlupek
* All rights reserved
* info@monoflow.org
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using OSCsharp.Data;


namespace UniOSC{

	/// <summary>
	/// Dispatcher button that forces a OSCConnection to send a OSC Message.
	/// Two separate states: Down and Up 
	/// </summary>
	public class OSCSender: UniOSCEventDispatcher {

		#region public
		#endregion

		#region private
		private bool _btnDown;
		private GUIStyle _gs; 
		#endregion

		public override void Awake()
		{
			base.Awake ();
			
		}

		public override void OnEnable ()
		{
			base.OnEnable ();
            ClearData();
            AppendData(0);
           

		}
		public override void OnDisable ()
		{
			base.OnDisable ();
		}

		/// <summary>
		/// Sends the OSC message with the downOSCDataValue.
		/// </summary>
		public void SendOSCMessage(int _id){
			if(_OSCeArg.Packet is OscMessage)
			{
                ((OscMessage)_OSCeArg.Packet).UpdateDataAt(0, _id);
            }
			else if(_OSCeArg.Packet is OscBundle)
			{
                foreach (OscMessage m in ((OscBundle)_OSCeArg.Packet).Messages)
                {
                    m.UpdateDataAt(0, _id);
                }				
			}

			_SendOSCMessage(_OSCeArg);
		}

	}
}