﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace CasaSeat
{

    public class GraphicsOverlayController : MonoBehaviour
    {
		public event Action<GraphicOverlays> OnGraphicOverlayChange;
		private static GraphicsOverlayController _instance;
		public static GraphicsOverlayController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<GraphicsOverlayController>();

					if (_instance == null)
					{
						GameObject container = new GameObject("GRAPHICS OVERLAY CONTROLLER");
						_instance = container.AddComponent<GraphicsOverlayController>();
					}
				}

				return _instance;
			}
		}

		private GraphicOverlays currentGraphicsOverlay, previousGraphicsOverlay;
		private StatesController statesController;
		private ModesController modesController;

		public int characterOverlayIndex = 0;

		void Start()
		{
			statesController = StatesController.Instance;
			statesController.OnAppStateChange += OnStateChange;

			modesController = ModesController.Instance;
		}

		public void SetGraphicsOverlay(GraphicOverlays overlay)
		{
            previousGraphicsOverlay = currentGraphicsOverlay;
			currentGraphicsOverlay = overlay;
			if (OnGraphicOverlayChange != null) OnGraphicOverlayChange(currentGraphicsOverlay);
            OSCController.SendMessage(OSCController.OSC_MSG.TEXT_ON_SCREEN, (int)overlay);
        }

		public GraphicOverlays GetcurrentGraphicsOverlay()
		{
			return currentGraphicsOverlay;
		}

		public GraphicOverlays GetpreviousGraphicsOverlay()
		{
			return previousGraphicsOverlay;
		}

		public void SetInitialGraphicsOverlay(GraphicOverlays overlay)
		{
			previousGraphicsOverlay = overlay;
			currentGraphicsOverlay = overlay;
		}

		// TODO: REMOVE AFTER DELEVELOPEMENT
		void Update()
		{
            /*
			if(Input.GetKeyDown(KeyCode.Q)) SetGraphicsOverlay(GraphicOverlays.HelloOverlay);
			else if(Input.GetKeyDown(KeyCode.W)) SetGraphicsOverlay(GraphicOverlays.Word1Overlay);
			else if(Input.GetKeyDown(KeyCode.E)) SetGraphicsOverlay(GraphicOverlays.Word2Overlay);
			else if(Input.GetKeyDown(KeyCode.R)) SetGraphicsOverlay(GraphicOverlays.Word3Overlay);
			else if(Input.GetKeyDown(KeyCode.T)) SetGraphicsOverlay(GraphicOverlays.MobilityDataOverlay);
			else if(Input.GetKeyDown(KeyCode.Y)) SetGraphicsOverlay(GraphicOverlays.WeatherDataOverlay);
			else if(Input.GetKeyDown(KeyCode.U)) SetGraphicsOverlay(GraphicOverlays.CultureDataOverlay);
			else if(Input.GetKeyDown(KeyCode.I)) SetGraphicsOverlay(GraphicOverlays.ByeOverlay);
			else if(Input.GetKeyDown(KeyCode.O)) SetGraphicsOverlay(GraphicOverlays.NoOverlay);
            */
		}


		void OnStateChange(AppStates state)
		{
			if(state == AppStates.Overlay)
			{
                //Debug.Log("GraphicsOverlayController.OnStateChange: " + characterOverlayIndex);
				if(modesController.GetCurrentMode() == AppModes.Interaction)
				{
					if(characterOverlayIndex == 0) SetGraphicsOverlay(GraphicOverlays.Word1Overlay);
					else if(characterOverlayIndex == 1) SetGraphicsOverlay(GraphicOverlays.Word2Overlay);
					else if(characterOverlayIndex == 2) SetGraphicsOverlay(GraphicOverlays.Word3Overlay);
                }
				else if(modesController.GetCurrentMode() == AppModes.Standby || modesController.GetCurrentMode() == AppModes.ForceStandby)
				{
					if(characterOverlayIndex == 0) SetGraphicsOverlay(GraphicOverlays.Mobility);
					else if(characterOverlayIndex == 1) SetGraphicsOverlay(GraphicOverlays.Clima);
					else if(characterOverlayIndex == 2) SetGraphicsOverlay(GraphicOverlays.Events);
				}
				characterOverlayIndex++;
			} 
			else if(state == AppStates.Hello) SetGraphicsOverlay(GraphicOverlays.HelloOverlay);
			else if(state == AppStates.Selfie) SetGraphicsOverlay(GraphicOverlays.SelfieOverlay);
			else if(state == AppStates.Bye) SetGraphicsOverlay(GraphicOverlays.ByeOverlay);
			else if(state == AppStates.FinishStandby) SetGraphicsOverlay(GraphicOverlays.NoOverlay);
		}
		
	}

	public enum GraphicOverlays
	{
		HelloOverlay,
		CharacterNumOverlay,
		Word1Overlay,
        Word2Overlay,
        Word3Overlay,
        Mobility,
        Clima,
        Events,
		SelfieOverlay,
        ByeOverlay,
        NoOverlay
	}
}
