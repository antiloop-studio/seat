﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;

namespace CasaSeat
{
    public class ServerDataController : MonoBehaviour
    {
        public bool isTest;
        public string apiEndPoint;
        public string apiEndPointTest;
        public static bool hasData = false;
        private static ServerDataController _instance;
        public static ServerDataController Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<ServerDataController>();
                    
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("SERVER DATA CONTROLLER");
                        _instance = container.AddComponent<ServerDataController>();
                    }
                }
            
                return _instance;
            }
        }
        public event Action<DomesticApiData> serverDataUpdated; 
		public static DomesticApiData overlayData;

        private void Start()
        {
            InvokeRepeating("UpdateData", 0, 5);
        }

        public void UpdateData()
		{
			StartCoroutine(GetRequest(isTest ? apiEndPointTest : apiEndPoint));
		}

        IEnumerator GetRequest(string uri)
        {
            //Debug.Log("ServerDataController.GetRequest");
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                yield return webRequest.SendWebRequest();

                if (!webRequest.isNetworkError)
                {
                    overlayData = JsonConvert.DeserializeObject<DomesticApiData>(webRequest.downloadHandler.text);
                    overlayData.available_copies.Events.overlay_8 = null;
                    //Debug.Log("ServerDataController.GetRequest SUCCESS: " + overlayData);
                    hasData = true;
                    if (serverDataUpdated != null) serverDataUpdated(overlayData);
                    OSCController.SendMessage(OSCController.OSC_MSG.VOLUME_LEVEL, overlayData.soundLevel);
                }
            }
        }
    }

	public struct DomesticApiData
	{
		public OverlayData available_copies;
		public AppStyle schedule;
        public VideosPlaylist playlist;
        public int soundLevel;
    }

    public struct VideosPlaylist
    {
        public string name;
        public PlaylistVideo[] videos;
    }

    public struct PlaylistVideo
    {
        public string id;
        public int position;
        public PlaylistVideoItem video;
    }

    public struct PlaylistVideoItem
    {
        public string id;
        public string name;
        public string file;
    }

    public struct AppStyle
	{
		public bool cicle;
		public bool shared;
		public bool interactive;
	}

	public struct MobilityOverlaysContainer
	{
		public CopyData[] overlay_1;
		public CopyData[] overlay_2;
		public CopyData[] overlay_3;
	}

	public struct ClimateOverlaysContainer
	{
		public CopyData[] overlay_4;
	}

	public struct EventsOverlaysContainer
	{
		public CopyData[] overlay_5;
		public CopyData[] overlay_6;
		public CopyData[] overlay_3;
		public CopyData[] overlay_7;
		public CopyData[] overlay_8;
	}

	public struct CopyData
	{
		public string main;
		public string data;
	}


	public struct OverlayData
	{
		public string[] Interaction_welcome;
		public string[] Interaction_action;
		public string[] Interaction_closure;
		public MobilityOverlaysContainer Mobility;
		public ClimateOverlaysContainer Clima;
		public EventsOverlaysContainer Events;
	} 
}
