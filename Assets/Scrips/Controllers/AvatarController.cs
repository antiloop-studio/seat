﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
	public class AvatarController : MonoBehaviour
    {

        private static AvatarController _instance;
        public static AvatarController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<AvatarController>();

                }

                return _instance;
            }
        }

        public GameObject[] ObjectsToHideUnbuild;

        public Transform spheresParent;
        public Animator avatarAnimator;
        public SkinnedMeshRenderer animCharRenderer;
        [Range(0, 1)]
        public float sourceSkeletonWeight;
        [Header("movespeed in meters per second")]
        [Range(0.1f, 5)]
        public float MoveSpeed;
        public float rotateSpeed = 5;
        public AvatarSkeleton kinectAvatar, animateAvatar;

        [Header("Ground correction")]
        public bool isGrounded;
        public Transform avatarRoot, avatarHips, avatarFootBaseA, avatarFootBaseB, kinectFootA, kinectFootB, animateFootA, animateFootB;
        public ScaleBodyPart[] bodyParts;
        public LimbController[] limbs;
        public GameObject bodyMesh;

		public ObjectToTransform headSphere;
		public ObjectToTransform[] shoulderSpheres;
		public ObjectToTransform[] hipSpheres;

		public NecklaceController neckless;
		public Torus[] rings;

		public Transform[] bodyThicknessTransformsUp;
        public Transform[] bodyThicknessTransformsMiddle;
        public Transform[] bodyThicknessTransformsDown;
        public UpdateWaistTransforms waistTransforms;

        public float shadowSizeMult;
        public float shadowSizeDist = 1;
        public float shadowSizeDistMult = 1;
        public Transform footLeft, footRight;
        public Transform shadowFootLeft, shadowFootRight;

        private StatesController statesController;
        private ModesController modesController;
        private AppearanceController appearanceController;
        private StandbyAvatarController standbyAvatarController;

        private ShapesBodypartController[] shapesAlongSpline;

        private Transform[] avatarJoints;
        private bool usingKinectSkeletonPosition = false, AnimateAvatarToKinect = false;
        private float lerpTime;
		private bool shouldUpdate = true;

        private Transform walkTarget = null;

        private float tubeClippingFrom = 0;
        private float tubeClippingTo = 1;

        public Slider sliderAvatarOffsetY; 
        private float avatarOffsetY = 0;
        private bool doChangeOffset = true;

        Vector3 LegPositionLeft;
        Vector3 LegPositionRight;

        private void Awake()
        {
            
        }

        void Start()
        {
            statesController        = StatesController.Instance;
            modesController         = ModesController.Instance;
            appearanceController    = AppearanceController.Instance;
            standbyAvatarController = StandbyAvatarController.Instance;

            modesController.OnAppModeChange += OnModeChange;
            statesController.OnAppStateChange += OnStateChange;

            shapesAlongSpline = spheresParent.GetComponentsInChildren<ShapesBodypartController>() as ShapesBodypartController[];

            kinectAvatar.InitAvatarSkeleton();
            animateAvatar.InitAvatarSkeleton();

            appearanceController.appearanceUpdaterStartChange += appearanceUpdaterStartChange;
            appearanceController.particleTypeChanged += particleTypeChanged;
            appearanceController.particleColliderChanged += particleColliderChanged;
            appearanceController.particlePhysicsChanged += particlePhysicsChanged;

            InitializeAvatar();

            StartCoroutine(RandomAnimation());

            if (PlayerPrefs.HasKey("avatarOffsetY"))
            {
                doChangeOffset = false;
                avatarOffsetY = PlayerPrefs.GetFloat("avatarOffsetY");
                sliderAvatarOffsetY.value = avatarOffsetY;
                doChangeOffset = true;
            }
        }

        public void CahngeAvatarOffset(float val)
        {
            avatarOffsetY = val;
            PlayerPrefs.SetFloat("avatarOffsetY", avatarOffsetY);
        }

        void OnModeChange(AppModes mode)
        {
            LeanTween.cancel(gameObject);
            Debug.Log("AvatarController.OnModeChange: " + mode + " // prev mode= " + modesController.GetPreviousMode());
            if (mode == modesController.GetPreviousMode()) return;

            if (mode == AppModes.SeatVideos)
            {
                shouldUpdate = false;
            }
            else
            {
                if (mode == AppModes.Standby)
                {
                    for (int i = 0; i < limbs.Length; i++)
                    {
                        limbs[i].splineMesh.SetClipRange(0, 1);
                    }
                    ShowUnbuildParts(true);
                }


                shouldUpdate = true;
                if (mode == AppModes.Interaction && modesController.GetPreviousMode() != AppModes.Interaction)
                {
                    walkTarget = kinectAvatar.root;
                }
                else if (modesController.GetPreviousMode() != AppModes.Standby || modesController.GetPreviousMode() != AppModes.ForceStandby)
                {
                    walkTarget = animateAvatar.root;
                    KinectTransition(1);
                }
                //WalkToLocation();
                WalkComplete();

            }
        }

        void OnStateChange(AppStates state)
        {
            LeanTween.cancel(gameObject);

            if (state == AppStates.Hello)
            {
                LeanTween.delayedCall(gameObject, 2f, () => {
                    foreach (var item in ObjectsToHideUnbuild)
                    {
                        item.gameObject.SetActive(true);
                    }

                    for (int i = 0; i < limbs.Length; i++)
                    {
                        limbs[i].splineMesh.SetClipRange(0, 0);
                    }

                    ShowUnbuildParts(true);
                });

                LeanTween.value(gameObject, 0, 1, 1.5f).setDelay(3.5f).setEase(LeanTweenType.easeInOutBack).setOnUpdate((float val) =>
                {
                    for (int i = 0; i < limbs.Length; i++)
                    {
                        limbs[i].splineMesh.SetClipRange(0, val);
                    }
                });
            }
            else if (state == AppStates.Bye)
            {

                LeanTween.value(gameObject, 1, 0, 1.5f).setDelay(0.5f).setEase(LeanTweenType.easeInOutBack).setOnUpdate((float val) =>
                {
                    for (int i = 0; i < limbs.Length; i++)
                    {
                        limbs[i].splineMesh.SetClipRange(tubeClippingFrom, val);
                    }
                });

                LeanTween.delayedCall(gameObject, 1f, () =>
                {
                    ObjectsToHideUnbuild[0].SetActive(false);
                });

                LeanTween.delayedCall(gameObject, 1.5f, () => { 
                    for (int i = 0; i < shapesAlongSpline.Length; i++)
                    {
                        shapesAlongSpline[i].SetGravity(false);
                    }
                    ShowUnbuildParts(false);
                });

            } else
            {
                for (int i = 0; i < limbs.Length; i++)
                {
                    limbs[i].splineMesh.SetClipRange(0, 1);
                }
                
            }
        }

        void ShowUnbuildParts(bool val)
        {
            foreach (var item in hipSpheres)
            {
                item.gameObject.SetActive(val);
            }
            foreach (var item in shoulderSpheres)
            {
                item.gameObject.SetActive(val);
            }

            foreach (var item in ObjectsToHideUnbuild)
            {
                item.gameObject.SetActive(val);
            }
            bodyMesh.SetActive(val);
        }

        IEnumerator RandomAnimation()
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(4, 8));
            SetAnimationById(UnityEngine.Random.Range(0, 13));
            StartCoroutine(RandomAnimation());
        }
        

        void InitializeAvatar()
        {
            avatarJoints = avatarRoot.GetChild(0).GetComponentsInChildren<Transform>();
            for (int i = 0; i < bodyParts.Length; i++)
            {
                bodyParts[i].InitScaleBodyPart();
            }
        }


        void Update()
        {
			if(shouldUpdate)
			{
				CharacterAppearanceData data = appearanceController.OutputAppearance;
				SetBodyPartsScale();
                
                SetHeadParameters();
				SetNecklessParameters();
				SetLimbRadiuses();
                
                SetParticleParams();
				SetJointRadiuses();
				SetBodyThickness();
				SetWaistParameters();

				UpdateAvatarSkeleton();
				UpdateAvatarScale();

				if (walkTarget != null) Walk();
			}

            SetShadows();

        }

        private void SetShadows()
        {
            float s = appearanceController.OutputAppearance.processedValues[(int)CHARACTER_MODIFIER.LEGS_BOTTOM_RADIUS] * shadowSizeMult + ((shadowSizeDist - LegPositionLeft.y) * shadowSizeDistMult);
            shadowFootLeft.transform.localScale = Vector3.one * s;
            shadowFootLeft.transform.position = new Vector3(LegPositionLeft.x, 0.01f, LegPositionLeft.z);
            s = appearanceController.OutputAppearance.processedValues[(int)CHARACTER_MODIFIER.LEGS_BOTTOM_RADIUS] * shadowSizeMult + ((shadowSizeDist - LegPositionRight.y) * shadowSizeDistMult);
            shadowFootRight.transform.localScale = Vector3.one * s;
            shadowFootRight.transform.position = new Vector3(LegPositionRight.x, 0.01f, LegPositionRight.z);

            LegPositionLeft = footLeft.position;
            LegPositionRight = footRight.position;
        }

       

        private void appearanceUpdaterStartChange(AppearanceUpdater obj)
        {
            for (int i = 0; i < shapesAlongSpline.Length; i++)
            {
                shapesAlongSpline[i].ExplodeParticles();
            }
        }

        private void particlePhysicsChanged(AppearanceUpdater _appearance)
        {
            for (int i = 0; i < shapesAlongSpline.Length; i++)
            {
                shapesAlongSpline[i].SetPhysics(_appearance.VisualData.particleHasPhysics);
            }
        }

        private void particleColliderChanged(AppearanceUpdater _appearance)
        {
            for (int i = 0; i < shapesAlongSpline.Length; i++)
            {
                shapesAlongSpline[i].SetCollider(_appearance.VisualData.particleHasCollider);
            }
        }


        private void particleTypeChanged(AppearanceUpdater _appearance)
        {
            for (int i = 0; i < shapesAlongSpline.Length; i++)
            {
                shapesAlongSpline[i].appearance = _appearance;
                shapesAlongSpline[i].EraseParticles();
            }
        }

        void SetBodyThickness()
		{
            CharacterAppearanceData data = appearanceController.OutputAppearance;
            for (int i = 0; i < bodyThicknessTransformsUp.Length; i++)
			{
                bodyThicknessTransformsUp[i].localScale = new Vector3(0.001f, 0.001f, data.processedValues[(int)CHARACTER_MODIFIER.TORSO_UP]);
			}
            for (int i = 0; i < bodyThicknessTransformsMiddle.Length; i++)
            {
                bodyThicknessTransformsMiddle[i].localScale = new Vector3(0.001f, 0.001f, data.processedValues[(int)CHARACTER_MODIFIER.TORSO_MIDDLE]);
            }
            for (int i = 0; i < bodyThicknessTransformsDown.Length; i++)
            {
                bodyThicknessTransformsDown[i].localScale = new Vector3(0.001f, 0.001f, data.processedValues[(int)CHARACTER_MODIFIER.TORSO_DOWN]);
            }
        }

		void SetWaistParameters()
		{
            CharacterAppearanceData data = appearanceController.OutputAppearance;
            waistTransforms.waistWidth = data.processedValues[(int)CHARACTER_MODIFIER.WAIST_WIDTH];
            waistTransforms.waistHeight = data.processedValues[(int)CHARACTER_MODIFIER.WAIST_HEIGHT];
        }

		void SetHeadParameters()
		{
            CharacterAppearanceData data = appearanceController.OutputAppearance;
            headSphere.size = data.processedValues[(int)CHARACTER_MODIFIER.HEAD_RADIUS];
			headSphere.objectOffset = new Vector3(0, data.processedValues[(int)CHARACTER_MODIFIER.HEAD_OFFSET], 0);
		}

		void SetNecklessParameters()
		{
            CharacterAppearanceData data = appearanceController.OutputAppearance;

            rings[0].gameObject.SetActive(data.hasTopRing);
            rings[1].gameObject.SetActive(data.hasBottomRing);

            if (neckless.ratios[0] != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_HEIGHT]) neckless.ratios[0] = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_HEIGHT];
            if (rings[0].CurveRadius != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_RADIUS]) rings[0].CurveRadius = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_RADIUS];
            if (rings[0].TorusRadius != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_CURVE_RADIUS]) rings[0].TorusRadius = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_TOP_CURVE_RADIUS];

            if (neckless.ratios[1] != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_HEIGHT]) neckless.ratios[1] = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_HEIGHT];
            if (rings[1].CurveRadius != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_RADIUS]) rings[1].CurveRadius = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_RADIUS];
            if (rings[1].TorusRadius != data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_CURVE_RADIUS]) rings[1].TorusRadius = data.processedValues[(int)CHARACTER_MODIFIER.NECK_RING_BOTTOM_CURVE_RADIUS];
        }

		void SetJointRadiuses()
		{
            CharacterAppearanceData data = appearanceController.OutputAppearance;
            foreach (ObjectToTransform obj in shoulderSpheres)
			{
				obj.size = data.processedValues[(int)CHARACTER_MODIFIER.SHOULDER_SPHERE_RADIUS];
			}

			foreach (ObjectToTransform obj in hipSpheres)
			{
				obj.size = data.processedValues[(int)CHARACTER_MODIFIER.HIP_SPHERE_RADIUS];
            }
		}


        void SetParticleParams()
        {
            CharacterAppearanceData data = appearanceController.OutputAppearance;

            for (int i = 0; i < shapesAlongSpline.Length; i++)
            {
                shapesAlongSpline[i].radius = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_OFFSET];
                shapesAlongSpline[i].minSize = data.originalValues[(int)CHARACTER_MODIFIER.PARTICLE_SIZE][0];
                shapesAlongSpline[i].maxSize = data.originalValues[(int)CHARACTER_MODIFIER.PARTICLE_SIZE][1];
                shapesAlongSpline[i].spacing = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_SPACING];

                shapesAlongSpline[i].particleBaseForce = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_BASE_FORCE];
                shapesAlongSpline[i].particleForceMult = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_FORCE_MULT];
                shapesAlongSpline[i].particleDistMult = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_DIST_MULT];
                shapesAlongSpline[i].particleHeadingEase = data.processedValues[(int)CHARACTER_MODIFIER.PARTICLE_HEADING_EASE];
            }
        }

        void SetLimbRadiuses()
        {
            CharacterAppearanceData data = appearanceController.OutputAppearance;


            limbs[0].startTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_TOP_RADIUS];
            limbs[0].midTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_MIDDLE_RADIUS];
            limbs[0].endTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_BOTTOM_RADIUS];

            limbs[1].startTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_TOP_RADIUS];
            limbs[1].midTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_MIDDLE_RADIUS];
            limbs[1].endTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.ARMS_BOTTOM_RADIUS];

            limbs[2].startTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_TOP_RADIUS];
            limbs[2].midTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_MID_RADIUS];
            limbs[2].endTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_BOTTOM_RADIUS];

            limbs[3].startTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_TOP_RADIUS];
            limbs[3].midTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_MID_RADIUS];
            limbs[3].endTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.LEGS_BOTTOM_RADIUS];

            limbs[4].startTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.NECK_BOTTOM_RADIUS];
            limbs[4].endTubeSizeMultiplier = data.processedValues[(int)CHARACTER_MODIFIER.NECK_TOP_RADIUS];

        }

        void SetBodyPartsScale()
        {
            CharacterAppearanceData data = appearanceController.OutputAppearance;

            bodyParts[0].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.NECK_OFFSET];
            bodyParts[1].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.SHOULDERS_X];
            bodyParts[1].scaleInfos[1].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.SHOULDERS_Y];
            bodyParts[2].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.TORSO];
            bodyParts[3].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.UPPER_ARMS];
            bodyParts[4].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.LOWER_ARMS];
            bodyParts[5].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.HIPS];
            bodyParts[6].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.UPPER_LEGS];
            bodyParts[7].scaleInfos[0].scaleAmount = data.processedValues[(int)CHARACTER_MODIFIER.LOWER_LEGS];
        }

        

        void WalkToLocation() {
            float d = Vector3.Distance(avatarRoot.position, walkTarget.position);
            if(d > 0.1f)
            {
                standbyAvatarController.PlayNewAnimation("Walk");
            }
        }

        void Walk() {
            avatarRoot.Translate(0, 0, MoveSpeed * Time.deltaTime);
            Vector3 direction = (walkTarget.position - avatarRoot.position).normalized; 
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            avatarRoot.rotation = Quaternion.Slerp(avatarRoot.rotation, lookRotation, Time.deltaTime * (lookRotation.eulerAngles.y / 180) * rotateSpeed);

            float d = Vector3.Distance(avatarRoot.position, walkTarget.position);
            if (d < 0.1f) {

                WalkComplete();
            }
        }

        void WalkComplete() {

            walkTarget = null;
            AppModes mode = modesController.GetCurrentMode();

            Debug.Log("AvatarController.WalkComplete: " + mode);

            float rotationTime = 1.0f * (Mathf.Abs(Mathf.DeltaAngle(avatarRoot.eulerAngles.y, kinectAvatar.root.eulerAngles.y)) / 360.0f);
            LeanTween.rotate(avatarRoot.gameObject, kinectAvatar.root.rotation.eulerAngles, rotationTime).setEaseOutQuad();

            if(mode == AppModes.PreStandby) {
                standbyAvatarController.PlayNewAnimation("Prestandby");
                
            } else if(mode == AppModes.Standby) {
                standbyAvatarController.PlayNewAnimation("Walk");
                
            } else if(mode == AppModes.Interaction) {
                standbyAvatarController.PlayNewAnimation("Idle");
                KinectTransition(0);
            }
        }

        void KinectTransition(float target) {
            LeanTween.value(sourceSkeletonWeight, target, 0.5f).setEaseInOutQuad().setOnUpdate((float val) => {
                sourceSkeletonWeight = val;
            }).setOnComplete(() => {
                usingKinectSkeletonPosition = true;
            });
        }

        

        void UpdateAvatarScale()
        {
            for (int i = 0; i < bodyParts.Length; i++)
            {
                bodyParts[i].UpdateScale();
            }
        }

        void UpdateAvatarSkeleton()
        {
            for (int i = 0; i < avatarJoints.Length; i++)
            {
                avatarJoints[i].rotation = Quaternion.Lerp(kinectAvatar.joints[i].rotation, animateAvatar.joints[i].rotation, sourceSkeletonWeight);
            }

            float dist = 0;
            if (modesController.GetCurrentMode() == AppModes.Interaction) { 
                if (avatarFootBaseA.position.y < avatarFootBaseB.position.y)
                {
                    dist = kinectFootA.position.y - avatarFootBaseA.position.y;
                }
                else
                {
                    dist = kinectFootB.position.y - avatarFootBaseB.position.y;
                }
            } else if (modesController.GetCurrentMode() == AppModes.Standby)
            {
                if (avatarFootBaseA.position.y < avatarFootBaseB.position.y)
                {
                    dist = animateFootA.position.y - avatarFootBaseA.position.y;
                }
                else
                {
                    dist = animateFootB.position.y - avatarFootBaseB.position.y;
                }
            }
            dist += avatarOffsetY;
            avatarHips.position += new Vector3(0, dist, 0);

            /*
            if (avatarFootBaseA.position.y < avatarRoot.position.y || avatarFootBaseB.position.y < avatarRoot.position.y)
            {
                if(avatarFootBaseA.position.y < avatarFootBaseB.position.y)
                {
                    dist = avatarRoot.position.y - avatarFootBaseA.position.y;
                }
                else
                {
                    dist = avatarRoot.position.y - avatarFootBaseB.position.y;
                }
                avatarHips.position += new Vector3(0, dist, 0);
            }

            else if(avatarFootBaseA.position.y > avatarRoot.position.y && avatarFootBaseB.position.y > avatarRoot.position.y)
            {
                if(avatarFootBaseA.position.y < avatarFootBaseB.position.y)
                {
                    dist = avatarFootBaseA.position.y - avatarRoot.position.y;
                }
                else
                {
                    dist = avatarFootBaseB.position.y - avatarRoot.position.y;
                }
                avatarHips.position -= new Vector3(0, dist, 0);
            }
            */
        }

        void SetAvatarRoot(bool useKinectRoot)
        {
            if(useKinectRoot)
            {
                avatarRoot.position = kinectAvatar.root.position;
                avatarRoot.rotation = kinectAvatar.root.rotation;
            }
            else
            {
                avatarRoot.position = animateAvatar.root.position;
                avatarRoot.rotation = animateAvatar.root.rotation;
            }
        }

        public void SetAnimationById(int _id)
        {
            avatarAnimator.SetInteger("AnimationId", _id);
            StartCoroutine(CRResetAnimation());
        }

        IEnumerator CRResetAnimation()
        {
            yield return new WaitForEndOfFrame();
            avatarAnimator.SetInteger("AnimationId", -1);
        }

        public void ShowAnimChar(bool val)
        {
            animCharRenderer.enabled = val;
        }
    }
}
