﻿using UnityEngine;
using Dreamteck.Splines;

public class LimbController : MonoBehaviour
{
    public float baseTubeSize; 
    public float startTubeSizeMultiplier, midTubeSizeMultiplier, endTubeSizeMultiplier;
    public Material tubeMaterial;
    public Transform[] splineControlTransforms;

    public SplineComputer spline;
    public TubeGenerator splineMesh;
    private MeshRenderer tubeRenderer;
    private SplinePoint[] splinePoints;

    private bool rebuildMesh = true;
	private bool hasThirdSizeModifier = false;

    void Start()
    {
		if(splineControlTransforms.Length >= 3) hasThirdSizeModifier = true;

        spline = GetComponent<SplineComputer>();
        splineMesh = GetComponent<TubeGenerator>() as TubeGenerator;

        splineMesh.sizeModifier.AddKey(0, 1);
        splineMesh.sizeModifier.keys[0].size = startTubeSizeMultiplier;
        splineMesh.sizeModifier.keys[0].globalCenterStart = 0;
        splineMesh.sizeModifier.keys[0].globalCenterEnd = 0;

		if(hasThirdSizeModifier)
		{
			splineMesh.sizeModifier.AddKey(0, 1);
			splineMesh.sizeModifier.keys[1].size = midTubeSizeMultiplier;
			splineMesh.sizeModifier.keys[1].globalCenterEnd = .5;
			splineMesh.sizeModifier.keys[1].globalCenterStart = .5;

			splineMesh.sizeModifier.AddKey(0, 1);
			splineMesh.sizeModifier.keys[2].size = endTubeSizeMultiplier;
			splineMesh.sizeModifier.keys[2].globalCenterEnd = 1;
			splineMesh.sizeModifier.keys[2].globalCenterStart = 1;
		}
		else
		{
			splineMesh.sizeModifier.AddKey(0, 1);
			splineMesh.sizeModifier.keys[1].size = endTubeSizeMultiplier;
			splineMesh.sizeModifier.keys[1].globalCenterEnd = 1;
			splineMesh.sizeModifier.keys[1].globalCenterStart = 1;
		}

        splinePoints = new SplinePoint[splineControlTransforms.Length];
        for (int i = 0; i < splineControlTransforms.Length; i++)
        {
            splinePoints[i] = new SplinePoint(splineControlTransforms[i].position);
        }
        

        tubeRenderer = gameObject.GetComponent<MeshRenderer>() as MeshRenderer;
        tubeRenderer.material = tubeMaterial;

    }

    void Update()
    {

		// Debug.Log(startTubeSizeMultiplier + " " + midTubeSizeMultiplier + " " + endTubeSizeMultiplier);

        UpdateSplineShape();

		if(hasThirdSizeModifier)
		{
			UpdateSizeModifiers();
			if(splineMesh.sizeModifier.keys[0].size != startTubeSizeMultiplier || splineMesh.sizeModifier.keys[1].size != midTubeSizeMultiplier || splineMesh.sizeModifier.keys[2].size != endTubeSizeMultiplier)
			{
				splineMesh.sizeModifier.keys[0].size = Mathf.Clamp(startTubeSizeMultiplier, 0, Mathf.Infinity);
				splineMesh.sizeModifier.keys[1].size = Mathf.Clamp(midTubeSizeMultiplier, 0, Mathf.Infinity);
				splineMesh.sizeModifier.keys[2].size = Mathf.Clamp(endTubeSizeMultiplier, 0, Mathf.Infinity);
				rebuildMesh = true;
			}
		}
		else
		{
			if(splineMesh.sizeModifier.keys[0].size != startTubeSizeMultiplier || splineMesh.sizeModifier.keys[1].size != endTubeSizeMultiplier)
			{
				splineMesh.sizeModifier.keys[0].size = Mathf.Clamp(startTubeSizeMultiplier, 0, Mathf.Infinity);
				splineMesh.sizeModifier.keys[1].size = Mathf.Clamp(endTubeSizeMultiplier, 0, Mathf.Infinity);
				rebuildMesh = true;
			}
		}	

        if(splineMesh.size != baseTubeSize)
        {
            splineMesh.size = baseTubeSize;
            rebuildMesh = true;
        }

        if(rebuildMesh) 
        {
            splineMesh.Rebuild();
            rebuildMesh = false;
        }

        //spline.RunUpdate();
        //splineMesh.RunUpdate();

    }

	void UpdateSizeModifiers()
	{
		double centerPercent = spline.Project(splinePoints[1].position).percent;

		splineMesh.sizeModifier.keys[0].end = centerPercent;
		splineMesh.sizeModifier.keys[1].globalCenterEnd = centerPercent;
		splineMesh.sizeModifier.keys[1].globalCenterStart = centerPercent;
		splineMesh.sizeModifier.keys[2].start = centerPercent;
	}

    void UpdateSplineShape()
    {
        for (int i = 0; i < splineControlTransforms.Length; i++)
        {
            splinePoints[i].SetPosition(splineControlTransforms[i].position);
            //splinePoints[i].normal = new Vector3(0, 0, 0);
            spline.SetPointPosition(i, splineControlTransforms[i].position, Dreamteck.Splines.SplineComputer.Space.World);
        }
        

    }
}
