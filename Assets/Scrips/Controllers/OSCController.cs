﻿using CasaSeat;
using System.Collections;
using System.Collections.Generic;
using UniOSC;
using UnityEngine;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using UnityEngine.UI;
using Strobotnik.GUA;

public class OSCController : MonoBehaviour
{
    public static OSCController instance;

    public enum OSC_MSG
    {
        USER_START,
        USER_END,
        LEFT_HAND_UP,
        RIGHT_HAND_UP,
        BOTH_HANDS_UP,
        JUMP,
        FLOOR_TOUCH,
        X_SHAPE,
        TEXT_ON_SCREEN,
        TEXT_SCROLL,
        NUM_COUNTER,
        BACKGROUND_CHANGE,
        VOLUME_LEVEL
    }
    public bool isLocalHost;
    public UniOSCConnection connection;
    public OSCSender sender;

    public CanvasGroup alexCanvas;
    public Image imgAlex;
    public Sprite[] spritesAlex;

    public Transform refHead;
    public Transform refLeftHand;
    public Transform refRightHand;
    public Transform refLeftFoot;
    public Transform refRightFoot;

    private float lastLeftHandActivated;
    private float lastRightHandActivated;
    private float lastBothHandsActivated;
    private float lastFloorTouchActivated;
    private float lastXShape;
    private float lastJump;

    private bool isLeftHandUp;
    private bool isRightHandUp;
    private bool isFloorTouch;

    private float repeatTime = 0.5f;

    private int indexAlex = 0;

    private List<int> easterEggCombination = new List<int> {0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1};
    //private List<int> easterEggCombination = new List<int> {0,1,0,1};
    private List<int> handInteractions = new List<int>();
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (isLocalHost) { 
            connection.oscOutIPAddress = GetIP(ADDRESSFAM.IPv4);
        }
        connection.ConnectOSCOut();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShowAlex();
        }

        if (ModesController.Instance.GetCurrentMode() != AppModes.Interaction) return;

        //BOTH HANDS
        if (refLeftHand.position.y > refHead.position.y && refRightHand.position.y > refHead.position.y && Time.time - lastBothHandsActivated > repeatTime && (!isLeftHandUp || !isRightHandUp))
        {
            lastBothHandsActivated = Time.time;
            lastRightHandActivated = Time.time;
            lastLeftHandActivated = Time.time;
            isLeftHandUp = true;
            isRightHandUp = true;
            SendMessage(OSC_MSG.BOTH_HANDS_UP);

            if (StatesController.Instance.GetCurrentState() != AppStates.Bye && !AppearanceController.Instance.lerpingAppearances) AppearanceController.Instance.NextPreset();
            AppManager.isAlex = false;

            
        }

        if (!isLeftHandUp && refLeftHand.position.y > refHead.position.y && Time.time - lastLeftHandActivated > repeatTime)
        {
            isLeftHandUp = true;
            lastLeftHandActivated = Time.time;
            SendMessage(OSC_MSG.LEFT_HAND_UP);

            handInteractions.Add(0);
            CheckEasterEgg();
            ShowAlex();

        }

        if (!isRightHandUp && refRightHand.position.y > refHead.position.y && Time.time - lastRightHandActivated > repeatTime)
        {
            isRightHandUp = true;
            lastRightHandActivated = Time.time;
            SendMessage(OSC_MSG.RIGHT_HAND_UP);

            handInteractions.Add(1);
            CheckEasterEgg();
            ShowAlex();

        }

        if (isLeftHandUp && refLeftHand.position.y < refHead.position.y - 0.2f)
        {
            isLeftHandUp = false;
        }
        if (isRightHandUp && refRightHand.position.y < refHead.position.y - 0.2f)
        {
            isRightHandUp = false;
        }

        //FLOOR TOUCH
        if (!isFloorTouch && refLeftHand.position.y < 0.2f && refRightHand.position.y < 0.3f && Time.time - lastFloorTouchActivated > repeatTime)
        {
            lastFloorTouchActivated = Time.time;
            isFloorTouch = true;
            SendMessage(OSC_MSG.FLOOR_TOUCH);
        }

        if (isFloorTouch && refLeftHand.position.y > 0.3f && refRightHand.position.y > 0.3f)
        {
            isFloorTouch = false;
        }
        
    }

    void CheckEasterEgg()
    {
        Debug.Log(handInteractions.ToArray());
        if (handInteractions.Count < easterEggCombination.Count) return;

        if (handInteractions.Count > easterEggCombination.Count) handInteractions.RemoveRange(0, handInteractions.Count - easterEggCombination.Count);

        bool isEasterEgg = true;
        for (int i = 0; i < handInteractions.Count; i++)
        {
            if (handInteractions[i] != easterEggCombination[i]) isEasterEgg = false;
        }

        if (isEasterEgg) AppManager.isAlex = true;
    }

    public void ShowAlex()
    {
        if (!AppManager.isAlex) return;

        indexAlex++;
        if (indexAlex >= spritesAlex.Length) indexAlex = 0;
        imgAlex.color = new Color(1, 1, 1, 1);
        imgAlex.sprite = spritesAlex[indexAlex];

        alexCanvas.alpha = 1;
        LeanTween.cancel(alexCanvas.gameObject);
        LeanTween.alphaCanvas(alexCanvas, 0, 0.3f).setDelay(0.3f);
    }

    public void SendMessageById(int id)
    {
        SendMessage((OSC_MSG)id);
    }

    public static void SendMessage(OSC_MSG msgID, int _id = -1)
    {
        string _msg = "";
        switch (msgID)
        {
            case OSC_MSG.USER_START:
                UserStats.StartSession();
                _msg = "user_start";
                break;
            case OSC_MSG.USER_END:
                UserStats.EndSession();
                _msg = "user_end";
                break;
            case OSC_MSG.LEFT_HAND_UP:
                UserStats.HandInteraction(0);
                _msg = "left_hand_up";
                break;
            case OSC_MSG.RIGHT_HAND_UP:
                UserStats.HandInteraction(1);
                _msg = "right_hand_up";
                break;
            case OSC_MSG.BOTH_HANDS_UP:
                UserStats.HandInteraction(2);
                _msg = "both_hands_up";
                break;
            case OSC_MSG.JUMP:
                _msg = "jump";
                break;
            case OSC_MSG.FLOOR_TOUCH:
                _msg = "floor_touch";
                break;
            case OSC_MSG.X_SHAPE:
                _msg = "x_shape";
                break;
            case OSC_MSG.TEXT_ON_SCREEN:
                _msg = "text_on_screen";
                break;
            case OSC_MSG.TEXT_SCROLL:
                _msg = "text_scroll";
                break;
            case OSC_MSG.NUM_COUNTER:
                _msg = "num_counter";
                break;
            case OSC_MSG.BACKGROUND_CHANGE:
                _msg = "background_change";
                break;
            case OSC_MSG.VOLUME_LEVEL:
                _msg = "volume_level";
                break;
            default:
                break;
        }

        OSCController.instance.sender.oscOutAddress = "/" + _msg;
        OSCController.instance.sender.SendOSCMessage(_id);

    }

    public static string GetIP(ADDRESSFAM Addfam)
    {
        //Return null if ADDRESSFAM is Ipv6 but Os does not support it
        if (Addfam == ADDRESSFAM.IPv6 && !Socket.OSSupportsIPv6)
        {
            return null;
        }

        string output = "";

        foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            NetworkInterfaceType _type1 = NetworkInterfaceType.Wireless80211;
            NetworkInterfaceType _type2 = NetworkInterfaceType.Ethernet;

            if ((item.NetworkInterfaceType == _type1 || item.NetworkInterfaceType == _type2) && item.OperationalStatus == OperationalStatus.Up)
#endif 
            {
                foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                {
                    //IPv4
                    if (Addfam == ADDRESSFAM.IPv4)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            output = ip.Address.ToString();
                        }
                    }

                    //IPv6
                    else if (Addfam == ADDRESSFAM.IPv6)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetworkV6)
                        {
                            output = ip.Address.ToString();
                        }
                    }
                }
            }
        }
        return output;
    }
}

public enum ADDRESSFAM
{
    IPv4, IPv6
}