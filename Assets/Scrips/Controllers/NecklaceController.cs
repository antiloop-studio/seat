﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecklaceController : MonoBehaviour
{
    public Transform neck;
    public Transform head;
    public Transform stopperBottom;
    public Transform stopperTop;
    public AttractorCenterFollower[] toruses;
    public float[] ratios;

    void Start()
    {
        
    }

    void Update()
    {
        stopperBottom.position = new Vector3(neck.position.x, neck.position.y - 0.5f, neck.position.z);
        stopperTop.position = new Vector3(head.position.x, head.position.y + 0.5f, head.position.z);
        for (int i = 0; i < toruses.Length; i++)
        {
            Vector3 targetPos = Vector3.Lerp(neck.position, head.position, ratios[i]);
            toruses[i].targetPosition = targetPos;
            if (toruses[i].transform.position.y < neck.position.y || toruses[i].transform.position.y > head.position.y) {
                toruses[i].transform.position = targetPos;
            }
        }
    }

    void FixedUpdate() {

    }
}
