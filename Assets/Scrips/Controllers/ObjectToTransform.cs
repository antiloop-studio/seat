﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToTransform : MonoBehaviour
{
	public Transform transformRef;
	public GameObject visualObject;
	public Vector3 objectOffset;
	public float size = 1;

    // Start is called before the first frame update
    void Start()
    {
        visualObject = Instantiate(visualObject, transform.position, Quaternion.identity, transform);
		UpdateTransform();
    }

    // Update is called once per frame
    void Update()
    {
		UpdateTransform();
    }

	void UpdateTransform()
	{
		transform.position = transformRef.position;
		transform.rotation = transformRef.rotation;
		visualObject.transform.localPosition = objectOffset;
		visualObject.transform.localScale = Vector3.one * size;
	}
}
