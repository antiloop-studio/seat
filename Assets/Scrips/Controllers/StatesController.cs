﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;
namespace CasaSeat
{
	public class StatesController : MonoBehaviour
	{
		public float beforeFinishAmount;
        public int loopCountMax = 3;
		public StateDuration[] EInteractionDurations;
		public StateDuration[] EStandbyDurations;

		public event Action<AppStates> OnAppStateChange;
        public event Action OnLoopFinished;
		private static StatesController _instance;
		public static StatesController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<StatesController>();

					if (_instance == null)
					{
						GameObject container = new GameObject("STATES CONTROLLER");
						_instance = container.AddComponent<StatesController>();
					}
				}

				return _instance;
			}
		}

		private AppStates currentState, previousState;
		private ModesController modesController;
		private Dictionary<AppStates, float> InteractionDurations = new Dictionary<AppStates, float>();
		private Dictionary<AppStates, float> StandbyDurations = new Dictionary<AppStates, float>();

        private CarouselContentUpdater[] contentUpdaters;
        private int loopCount = 0;
		private int overlayId = 0;

        public float restartTimeMinutes;
        private float restartLastTime = 0;

		void Awake()
		{
			EditorToDict(EInteractionDurations, InteractionDurations);
			EditorToDict(EStandbyDurations, StandbyDurations);

            contentUpdaters = FindObjectsOfType<CarouselContentUpdater>();
            restartLastTime = Time.time;
        }

		void Start()
		{
			modesController = ModesController.Instance;
			modesController.OnAppModeChange += OnModeChange;
		}


        public AppStates GetCurrentState()
        {
            return currentState;
        }

        public AppStates GetPreviousState()
        {
            return previousState;
        }

        void EditorToDict(StateDuration[] entries, Dictionary<AppStates, float> dict)
		{
			foreach (var entry in entries)
			{
				dict.Add(entry.state, entry.time);
			}
		}

		public void SetState(AppStates state)
		{
            //Debug.Log("StatesController.SetState = " + state + " // onlyCharacterCount: " + overlayId);
            StopAllCoroutines();
            previousState = currentState;
			currentState = state;
			if (OnAppStateChange != null) OnAppStateChange(currentState);
			AppModes currentMode = modesController.GetCurrentMode();

            //INTERACTION
            if (currentMode == AppModes.Interaction)
            {
                if (state == AppStates.Hello)
                {
                    overlayId = 0;
                    GraphicsOverlayController.Instance.characterOverlayIndex = 0;

                    StartCoroutine(ChangeStateDelay(InteractionDurations[AppStates.Hello], AppStates.Overlay));

                } else if (state == AppStates.Overlay)
                {
                    if (overlayId < 2)
                    {
                        overlayId++;
                        StartCoroutine(ChangeStateDelay(InteractionDurations[AppStates.Overlay], AppStates.Overlay));
                    }
                    else
                    {
                        overlayId = 0;
                        GraphicsOverlayController.Instance.characterOverlayIndex = 0;
                        StartCoroutine(ChangeStateDelay(InteractionDurations[AppStates.Overlay], AppStates.Selfie));
                    }
                }
                else if (state == AppStates.Selfie)
                {
                    StartCoroutine(ChangeStateDelay(InteractionDurations[AppStates.Selfie], AppStates.Bye));
                }
                else if (state == AppStates.Bye)
                {
                    UserStats.UserFinished();
                    overlayId = 0;
                    GraphicsOverlayController.Instance.characterOverlayIndex = 0;
                    if (OnLoopFinished != null) OnLoopFinished.Invoke();

                    StartCoroutine(ChangeStateDelay(InteractionDurations[AppStates.Bye], AppStates.Hello));
                }
            } else
            //STANDBY
            {
                if (state == AppStates.Overlay)
                {
                    if (overlayId < 2)
                    {
                        overlayId++;
                        StartCoroutine(ChangeStateDelay(StandbyDurations[AppStates.Overlay], AppStates.Overlay));
                    }
                    else
                    {
                        if (OnLoopFinished != null) OnLoopFinished.Invoke();
                        overlayId = 0;
                        GraphicsOverlayController.Instance.characterOverlayIndex = 0;
                        loopCount++;
                        Debug.Log("STANDBY FINISHED LOOP: " + loopCount);
                        if (loopCount == loopCountMax) { 
                            loopCount = 0;
                            StartCoroutine(ChangeStateDelay(StandbyDurations[AppStates.Overlay], AppStates.FinishStandby));
                        } else
                        {
                            StartCoroutine(ChangeStateDelay(StandbyDurations[AppStates.Overlay], AppStates.Overlay));
                            Debug.Log("RESTART - ELAPSED TIME:" + (Time.time - restartLastTime) + " // REMAINING TIME: " + ((restartTimeMinutes * 60) - (Time.time - restartLastTime)));
                            if (Time.time - restartLastTime > restartTimeMinutes * 60)
                            {
                                Debug.Log("!!! RESTART");
                                Application.LoadLevel(0);
                            }
                        }

                    }
                } else if (state == AppStates.FinishStandby)
                {
                    if (ServerDataController.hasData && (ServerDataController.overlayData.schedule.shared || ServerDataController.overlayData.schedule.cicle))
                    {
                        ModesController.Instance.SetMode(AppModes.SeatVideos, true);
                    }
                    else
                    {
                        SetState(AppStates.Overlay);
                    }
                }
            }

        }

		void ChangeCharacterState(Dictionary<AppStates, float> durations)
		{
            AppModes currentMode = modesController.GetCurrentMode();
            Debug.Log("StatesController.ChangeCharacterState: " + overlayId);

            int animNum = currentMode == AppModes.Interaction ? 3 : 2;
            if (overlayId < animNum)
			{
				StartCoroutine(ChangeStateDelay(durations[currentState], AppStates.Overlay));
				overlayId++;
			}
			else
			{
				overlayId = 0;
                GraphicsOverlayController.Instance.characterOverlayIndex = 0;
                if (currentMode == AppModes.Interaction) StartCoroutine(ChangeStateDelay(durations[currentState], AppStates.Bye));
                else if (currentMode == AppModes.Standby || currentMode == AppModes.ForceStandby)
                {
                    //finishNotification.NotifyFinish(durations[currentState]);
                    StartCoroutine(ChangeStateDelay(durations[currentState], AppStates.Overlay));
                }
			}
		}

		// TODO: REMOVE AFTER DELEVELOPEMENT
		void Update()
		{
			if(Input.GetKeyDown(KeyCode.A)) SetState(AppStates.Hello);
			else if(Input.GetKeyDown(KeyCode.F)) SetState(AppStates.Overlay);
			else if(Input.GetKeyDown(KeyCode.H)) SetState(AppStates.Bye);
		}

		void OnModeChange(AppModes mode)
		{
            Debug.Log("StatesController.ModeChange = " + mode);
			if(mode == AppModes.Standby || mode == AppModes.ForceStandby || mode == AppModes.Interaction) StartExperience(mode);
		}

		public void StartExperience(AppModes mode)
		{
			StopAllCoroutines();
			overlayId = 0;
            GraphicsOverlayController.Instance.characterOverlayIndex = 0;

            if (mode == AppModes.Interaction) SetState(AppStates.Hello);
			else if(mode == AppModes.Standby || mode == AppModes.ForceStandby) SetState(AppStates.Overlay);
		}

		public void StopExperience()
		{
			StopAllCoroutines();
			overlayId = 0;
		}

		IEnumerator ResetAppDelay(float delay)
		{
			yield return new WaitForSeconds(delay);
			StartExperience(modesController.GetCurrentMode());
		}

		IEnumerator ChangeStateDelay(float delay, AppStates state)
		{
			yield return new WaitForSeconds(delay);
			SetState(state);
		}

		[System.Serializable]
		public struct StateDuration
		{
			public AppStates state;
			public float time;
		}
	}

	public enum AppStates
	{
		Hello,
		Overlay,
        Selfie,
		Bye,
        FinishStandby
	}
}