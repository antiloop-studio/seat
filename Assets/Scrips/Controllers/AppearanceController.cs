﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
    public class AppearanceController : MonoBehaviour
    {
        public int presetId = 0;
        public int appearancesCount;
        public Slider appearanceSelector;
        public InputField inputAppearanceId;
        public Button btnRandom;
        public Button btnCopy;
        public Button btnPaste;
        public float visualStateChangeSpeed;
        public LeanTweenType easingType;
        public CameraController cameraController;

        public event Action<AppearanceUpdater> UpdateUI;
        public event Action<AppearanceUpdater> appearanceUpdaterStartChange;
        public event Action<AppearanceUpdater> appearanceUpdaterChanged;
        public event Action<AppearanceUpdater> particleTypeChanged;
        public event Action<AppearanceUpdater> particleColliderChanged;
        public event Action<AppearanceUpdater> particlePhysicsChanged;
        public event Action<AppearanceUpdater> ringChanged;

        public CharacterAppearanceData OutputAppearance { get; private set; }

        private static AppearanceController _instance;
        public static AppearanceController Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<AppearanceController>();
                    
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("APPEARANCE CONTROLLER");
                        _instance = container.AddComponent<AppearanceController>();
                    }
                }
            
                return _instance;
            }
        }
        
        private string dataFolder;
        private CharacterAppearanceData goalAppearance;
        private AppearanceUpdater[] appearanceUpdaters;
        private AppearanceUpdater currentAppearanceUpdater, goalAppearanceUpdater;
        public bool lerpingAppearances = false;
        private float t;
        private bool isIDChangesSlider = true;
        private bool isSliderChangesID = true;

        private CharacterAppearanceData dataToCopy;

        void Awake()
        {
            appearanceUpdaters = new AppearanceUpdater[appearancesCount];
            appearanceSelector.minValue = 0;
            appearanceSelector.maxValue = appearancesCount-1;
            appearanceSelector.wholeNumbers = true;

            
            appearanceSelector.onValueChanged.AddListener((float value) => {
                if (isSliderChangesID)
                {
                    isIDChangesSlider = false;
                    inputAppearanceId.text = value + "";
                    isIDChangesSlider = true;
                }
            });
            

            btnRandom.onClick.AddListener(OnRandomData);
            btnCopy.onClick.AddListener(OnCopyData);
            btnPaste.onClick.AddListener(OnPasteData);

            inputAppearanceId.onValueChanged.AddListener(OnAppearanceIdChanged);

            dataFolder = Application.streamingAssetsPath + "/CharacterAppearancesData/";

            if(!Directory.Exists(dataFolder))
            {
                Directory.CreateDirectory(dataFolder);
                for (int i = 0; i < appearancesCount; i++)
                {
                    AppearanceUpdater n = new AppearanceUpdater(i);
                    appearanceUpdaters[i] = n;
                    currentAppearanceUpdater = n;
                }

				SaveAppearances();

                currentAppearanceUpdater = appearanceUpdaters[0];
            }
            else
            {
                string[] dataFiles = Directory.GetFiles(dataFolder, "*.txt");
                if(dataFiles.Length != appearancesCount) throw new Exception("MISSING APPEARANCE FILES");
                for (int i = 0; i < dataFiles.Length; i++)
                {
                    AppearanceUpdater n = new AppearanceUpdater(i);
                    n.VisualData = JsonConvert.DeserializeObject<CharacterAppearanceData>(File.ReadAllText(dataFolder + "/appearance_" + i + ".txt"));
                    appearanceUpdaters[i] = n;
                }

                currentAppearanceUpdater = GetAppearanceUpdaterByIndex(0);
            }

            inputAppearanceId.text = "0";

            StatesController.Instance.OnAppStateChange += OnAppStateChange;
        }

        private void OnAppStateChange(AppStates _state)
        {
            if (_state == AppStates.Hello)
            {
                Invoke("NextPreset", 3.5f);
            } else if (_state == AppStates.Bye)
            {

            }
            else
            {
                NextPreset();
            }
        }

        public void NextPreset()
        {
            SetPreset(presetId + 1);
        }

        public void SliderEndDrag()
        {
            float _id = appearanceSelector.value;
            SetPreset((int)_id);
        }

        public void SetPreset(int _id)
        {
            if (_id == appearanceSelector.maxValue) _id = 0;
            if (_id < 0) _id = (int)appearanceSelector.maxValue - 1;

            presetId = _id;
            appearanceSelector.value = presetId;
            LerpToNewAppearanceUpdater(appearanceUpdaters[(int)presetId], visualStateChangeSpeed);
        }

        private void OnRandomData()
        {
            currentAppearanceUpdater.VisualData.ProcessValues();
            UpdateUI(currentAppearanceUpdater);
            SetParticleTypeChanged(currentAppearanceUpdater);
            SetParticleColliderChanged(currentAppearanceUpdater);
            SetParticlePhysicsChanged(currentAppearanceUpdater);
        }

        private void OnPasteData()
        {
            if (dataToCopy == null) return;

            CharacterAppearanceData _data = new CharacterAppearanceData();
            for (int i = 0; i < dataToCopy.originalValues.Length; i++)
            {
                _data.originalValues[i][0] = dataToCopy.originalValues[i][0];
                _data.originalValues[i][1] = dataToCopy.originalValues[i][1];
            }
            _data.ProcessValues();
            _data.particleHasCollider = dataToCopy.particleHasCollider;
            _data.particleHasPhysics = dataToCopy.particleHasPhysics;
            _data.hasTopRing = dataToCopy.hasTopRing;
            _data.hasBottomRing = dataToCopy.hasBottomRing;

            currentAppearanceUpdater.VisualData = _data;
            UpdateUI(currentAppearanceUpdater);
        }

        private void OnCopyData()
        {
            dataToCopy = currentAppearanceUpdater.VisualData;
            
        }

        private void OnAppearanceIdChanged(string val)
        {
            if (isIDChangesSlider)
            {
                int id;
                int.TryParse(val, out id);
                isSliderChangesID = false;
                appearanceSelector.value = id;
                isSliderChangesID = true;
            }
        }

        public void SaveAppearances()
        {
            if(!lerpingAppearances)
            {
                if(appearanceUpdaters.Length > 0)
                {
					for (int i = 0; i < appearanceUpdaters.Length; i++)
					{
						string json = JsonConvert.SerializeObject(appearanceUpdaters[i].VisualData);
						File.WriteAllText(dataFolder + appearanceUpdaters[i].appearanceName + ".txt", json);	
					}
                }
                else {
                    throw new SystemException("No Appearance data objects avaible to save to");
                }
            }
            else {
                throw new SystemException("Lerping between appearances..");
            }
        }

        void Update()
        {
            if(!lerpingAppearances)
            {
                OutputAppearance = currentAppearanceUpdater.VisualData;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                SetPreset(presetId + 1);
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                SetPreset(presetId - 1);
            }
        }

        public void LerpToNewAppearanceUpdater(AppearanceUpdater newAppearance, float lerpSpeed)
        {
            lerpingAppearances = true;
            goalAppearanceUpdater = newAppearance;
            visualStateChangeSpeed = lerpSpeed;
            LeanTween.cancel(gameObject);

            if(appearanceUpdaterStartChange != null) appearanceUpdaterStartChange(goalAppearanceUpdater);

            LeanTween.value(gameObject, 0, 1, visualStateChangeSpeed).setEase(easingType).setOnUpdate((float val) => {
                OutputAppearance = CharacterAppearanceData.Lerp(currentAppearanceUpdater.VisualData, goalAppearanceUpdater.VisualData, val);
                //cameraController.UpdateCamera();
            }).setOnComplete(() => {
                lerpingAppearances = false;
                OutputAppearance = goalAppearanceUpdater.VisualData;
                currentAppearanceUpdater = goalAppearanceUpdater;
                goalAppearanceUpdater = null;
                if(appearanceUpdaterChanged != null) appearanceUpdaterChanged(currentAppearanceUpdater);
            });

            if(UpdateUI != null) UpdateUI(newAppearance);
            SetParticleTypeChanged(newAppearance);
            SetParticleColliderChanged(newAppearance);
            SetParticlePhysicsChanged(newAppearance);
        }

        public void SetRingChanged(AppearanceUpdater _appearance)
        {
            if (ringChanged != null) ringChanged.Invoke(_appearance);
        }

        public void SetParticleTypeChanged(AppearanceUpdater _appearance)
        {
            if (particleTypeChanged != null) particleTypeChanged.Invoke(_appearance);
        }

        public void SetParticleColliderChanged(AppearanceUpdater _appearance)
        {
            if (particleColliderChanged != null) particleColliderChanged.Invoke(_appearance);
        }

        public void SetParticlePhysicsChanged(AppearanceUpdater _appearance)
        {
            if (particlePhysicsChanged != null) particlePhysicsChanged.Invoke(_appearance);
        }

        public AppearanceUpdater GetCurrentAppearanceUpdater()
        {
            return currentAppearanceUpdater;
        }

        public AppearanceUpdater GetAppearanceUpdaterByIndex(int index)
        {
            if(index >= 0 && index < appearancesCount) return appearanceUpdaters[index];
            else return null;
        }
    }
}

