﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformController : MonoBehaviour
{
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public MeshFilter bodyTargetMeshFilter;

    public Transform rightShoulder, leftShoulder, neckBase, rightHip, leftHip, waistLeft, waistRight;
    [Header("Do Not Modify")]
    public Transform meshRightShoulder, meshLeftShoulder, meshNeckBase, meshRightHip, meshLeftHip, meshLeftWaist, meshRightWaist;
    public float rotationRatio;

    private Mesh mesh;

    private void Start()
    {
        mesh = new Mesh();
    }
    // Update is called once per frame
    void Update()
    {

        UpdatePosRot(meshRightShoulder, rightShoulder);
        UpdatePosRot(meshRightHip, rightHip);
        UpdatePosRot(meshNeckBase, neckBase);
        UpdatePosRot(meshLeftHip, leftHip);
        UpdatePosRot(meshLeftShoulder, leftShoulder);
		UpdatePosRot(meshLeftWaist, waistLeft);
        UpdatePosRot(meshRightWaist, waistRight);

        Destroy(mesh);
        mesh = new Mesh();
        skinnedMeshRenderer.BakeMesh(mesh);
        mesh.RecalculateNormals();
        bodyTargetMeshFilter.mesh = mesh;

        
    }

    void UpdatePosRot(Transform to, Transform from)
    {
        to.position = from.position;
        //to.eulerE = from.rotation;
    }
}
