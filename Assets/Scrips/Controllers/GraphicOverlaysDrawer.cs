﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace CasaSeat
{
    public class GraphicOverlaysDrawer : MonoBehaviour
    {
		public string videosFolder, extension;

        public OverlayItem[] dynamicOverlayItems;

        private static GraphicOverlaysDrawer _instance;
        public static GraphicOverlaysDrawer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GraphicOverlaysDrawer>();

                    if (_instance == null)
                    {
                        GameObject container = new GameObject("GRAPHICS OVERLAY DRAWER");
                        _instance = container.AddComponent<GraphicOverlaysDrawer>();
                    }
                }

                return _instance;
            }
        }

        private GraphicsOverlayController graphicsController;
        private Dictionary<GraphicOverlays, OverlayContent> overlays;
		private VideoPlayerController staticOverlayPlayer;

		private string basePath = "file://";
        private ModesController modesController;

        void Start()
        {
            modesController = ModesController.Instance;
            modesController.OnAppModeChange += OnModeChange;

            graphicsController = GraphicsOverlayController.Instance;
            graphicsController.OnGraphicOverlayChange += OnGraphicOverlayChange;
			staticOverlayPlayer = VideoPlayerController.Instance;

            overlays = new Dictionary<GraphicOverlays, OverlayContent>();

			basePath += Application.dataPath + "/StreamingAssets/" + videosFolder;

            foreach (OverlayItem overlayItem in dynamicOverlayItems)
            {
                overlays.Add(overlayItem.overlayName, overlayItem.overlayContent);
            }
        }

        private void OnModeChange(AppModes _mode)
        {
            if (_mode == AppModes.Interaction) { 
                foreach (OverlayContent overlay in overlays.Values)
                {
                    if (overlay.isDynamic)
                    {
                        foreach (var slide in overlay.dynamicOverlays)
                        {
                            slide.ResetSlide();
                        }
                    }
                }
            }
        }

        public void HideAllOverlays()
        {
            foreach (OverlayContent overlay in overlays.Values)
            {
				if(overlay.isDynamic)
				{
					foreach (var slide in overlay.dynamicOverlays)
					{
						slide.ResetSlide();
					}
				}
            }

			staticOverlayPlayer.DisplayVideo(false);
        }

        void ShowOverlay(GraphicOverlays overlay)
        {
            HideAllOverlays();
            
            OverlayContent overlayContent;
            if(overlays.TryGetValue(overlay, out overlayContent))
			{
				if(overlayContent.isDynamic)
				{
					if(overlayContent.dynamicOverlays.Length == 0) return;
					SlideBase activeOverlay;
					if(overlayContent.pickRandom)
					{
						activeOverlay = overlayContent.dynamicOverlays[UnityEngine.Random.Range(0, overlayContent.dynamicOverlays.Length)];
					}
					else
					{
						activeOverlay = overlayContent.dynamicOverlays[overlayContent.index];
						if(overlayContent.index == overlayContent.dynamicOverlays.Length - 1) overlayContent.index = 0;
						else overlayContent.index++;
						overlays[overlay] = overlayContent;
					}
					activeOverlay.Show();
				}
				else
				{
					string videoName;
					if(overlayContent.staticOverlayNames.Length == 0) return;
					if(overlayContent.pickRandom)
					{
						videoName = basePath + overlayContent.staticOverlayNames[UnityEngine.Random.Range(0, overlayContent.staticOverlayNames.Length)] + extension;
					}
					else
					{
						videoName = basePath + overlayContent.staticOverlayNames[overlayContent.index] + extension;
						if(overlayContent.index == overlayContent.staticOverlayNames.Length - 1) overlayContent.index = 0;
						else overlayContent.index++;
						overlays[overlay] = overlayContent;
					}
					staticOverlayPlayer.DisplayVideo(true);
					staticOverlayPlayer.PlayVideo(videoName);
				}
			}
            else return;
        }

        void OnGraphicOverlayChange(GraphicOverlays overlay)
        {
			HideAllOverlays();
            if(overlay != GraphicOverlays.NoOverlay) ShowOverlay(overlay);
		}
    }

    [System.Serializable]
    public struct OverlayItem
    {
		public GraphicOverlays overlayName;
		public OverlayContent overlayContent;
	}

	[System.Serializable]
	public struct OverlayContent
	{
		public bool isDynamic, pickRandom;
		[HideInInspector]
		public int index;
        public SlideBase[] dynamicOverlays;
		public string[] staticOverlayNames;
	}
   
}