﻿using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
    public class StandbyAvatarController : MonoBehaviour
    {
        public Transform animateAvatarRoot;
        public Animator animationsController;
        public string[] animations;

        private static StandbyAvatarController _instance;
        public static StandbyAvatarController Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<StandbyAvatarController>();
                    
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("STANDBY AVATAR CONTROLLER");
                        _instance = container.AddComponent<StandbyAvatarController>();
                    }
                }
            
                return _instance;
            }
        }
        
        private StatesController statesController;
        private ModesController modesController;
        private bool animationPlaying = false;

        private Vector3 goalLocation, currentLocation;
        private float distance, lerpTime;
        private bool walking = false;

        void Start()
        {
            // animations = new string[]{"Idle", "NormalWalk", "SideWalk", "Wave", "RobotDance", "SalsaDance" };

            statesController = StatesController.Instance;
            statesController.OnAppStateChange += OnStateChange;
        }

        void Update()
        {
            if(walking)
            {
                // do the lerping here. 
            } 
        }

        public void PlayNewAnimation(string name)
        {
            Debug.Log("PLAY NEW ANIMATION = " + name);

            animationPlaying = true;
            animationsController.enabled = true;
            animationsController.SetFloat("Random", Random.Range(0f,1f));
            animationsController.SetTrigger(name);
        }

        public void StopPlayingAnimation()
        {
            animationPlaying = false;
            animationsController.enabled = false;
        }

        void OnStateChange(AppStates state)
        {
            // change the animtion that is currently happening. 
        }
    }

    public enum Animations
    {
        WalkingRight,
        WalkingLeft,
        More,
        Options,
        Idk
    }
}

