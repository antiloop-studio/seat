﻿using System.Collections;
using System.Collections.Generic;
using Dreamteck.Splines;
using UnityEngine;

public class SplineItem : MonoBehaviour
{
    public ShapesBodypartController splineController;
    public int index;
    public GameObject goRef;
    public bool hasCollider;
    public bool hasPhysycs;
    [HideInInspector]
    public Transform scaleGo;

    private GameObject containerDisplacement;
    private GameObject containerRotation;
    private SplineSample sample;
    private GameObject go;
    public Magnet magnet;
    private bool isScaling = false;

    // Start is called before the first frame update
    public void Init()
    {
        containerRotation = new GameObject();
        containerRotation.transform.parent = transform;
        containerRotation.name = "ROTATION";

        containerDisplacement = new GameObject();
        containerDisplacement.transform.parent = containerRotation.transform;
        containerDisplacement.name = "DISPLACEMENT";

        LateUpdate();

        go = Instantiate(goRef, Vector3.zero, Quaternion.identity);
        scaleGo = go.transform;
        if (splineController.hasAttractor) {
            go.transform.parent = splineController.transform;
            go.transform.position = containerDisplacement.transform.position;
            go.transform.rotation = containerDisplacement.transform.rotation;
            magnet = go.GetComponentInChildren<Magnet>();
            if (magnet) 
            {
                magnet.target = containerDisplacement.transform;
                magnet.splineController = splineController;
                magnet.magnetCollider.enabled = hasCollider;
                magnet.hasPhysics = hasPhysycs;
                //scaleGo = magnet.particleTransform;
            }
        } else {
            go.transform.parent = containerDisplacement.transform;
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            containerDisplacement.transform.localPosition = new Vector3(splineController.radius, 0, 0);
            go.GetComponentInChildren<MeshRenderer>().transform.localScale *= Random.Range(splineController.minSize, splineController.maxSize);
        }
        go.name = "REF";
        
        containerRotation.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360));

        go.transform.localScale = Vector3.zero;

        LeanTween.scale(go, Vector3.one * splineController.size, Random.Range(0f, 0.5f)).setOnComplete(() => {
            isScaling = true;
        });

    }

    // Update is called once per frame
    void LateUpdate()
    {
        float length = splineController.spline.CalculateLength();
        float dist = splineController.spacing * index;
        float ratio = dist / length;

        if (ratio > 1) {
            
        } else {
            Vector3 pos = splineController.spline.EvaluatePosition(ratio);
            transform.position = pos;
        }

        sample = splineController.spline.Evaluate(ratio);
        transform.rotation = sample.rotation;
        containerDisplacement.transform.localPosition = new Vector3(splineController.radius, 0, 0);

        if(isScaling && go && splineController) go.transform.localScale = Vector3.one * splineController.size;
    }

    public void UpdateFollowerParams(float bf, float fm, float dm, float he, float pr)
    {
        if(magnet)
        {
            magnet.baseForce = bf + Random.Range(-bf * pr, bf * pr);
            magnet.forceMult = fm + Random.Range(-fm * pr, fm * pr);
            magnet.distantMult = dm + Random.Range(-dm * pr, dm * pr);
            magnet.headingEase = he + Random.Range(-he * pr, he * pr);
        }
    }

    public void Remove() {
        isScaling = false;
        //go.transform.localScale = Vector3.one * splineController.size;
        StartCoroutine(DoDestroy(0.3f));
        LeanTween.cancel(go);
        LeanTween.scale(go, Vector3.zero, 0.3f);
    }

    public void Explode()
    {
        isScaling = false;
        if (magnet) magnet.Explode();

        float _delay = Random.Range(0f, 0.5f);
        StartCoroutine(DoDestroy(_delay + 1));

        LeanTween.cancel(go);
        LeanTween.scale(go, Vector3.zero, 1f).setDelay(_delay).setEase(LeanTweenType.easeInBack);
    }


    IEnumerator DoDestroy(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(go);
        Destroy(gameObject);
    }

}
