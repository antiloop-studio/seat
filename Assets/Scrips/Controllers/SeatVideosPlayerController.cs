﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video;
// using UnityEngine.UI;

namespace CasaSeat
{
	public class SeatVideosPlayerController : MonoBehaviour
	{
		public int videoDelay;
		public GameObject videoBackground;
		public GameObject logoContainer;

		private static SeatVideosPlayerController _instance;
		public static SeatVideosPlayerController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<SeatVideosPlayerController>();

					if (_instance == null)
					{
						GameObject container = new GameObject("SEAT VIDEOS PLAYER CONTROLLER");
						_instance = container.AddComponent<SeatVideosPlayerController>();
					}
				}

				return _instance;
			}
		}
        public Action OnFinishedLoop;
		private VideoPlayerController videoPlayer;
		private ModesController modesController;

		private string videosFolder;
		private string[] videoFiles;
		private int currentVideoIndex;

		void Start()
		{
			videoPlayer = VideoPlayerController.Instance;
			videoPlayer.videoFinished += OnVideoFinish;
            videoPlayer.videoError += OnVideoError;
            modesController = ModesController.Instance;
			modesController.OnAppModeChange += OnModeChange;

			videosFolder = Application.dataPath + "/StreamingAssets/SeatCompanyVideos/";
			videoFiles = Directory.GetFiles(videosFolder, "*.mp4");
		}

		void OnModeChange(AppModes mode)
		{
            Debug.Log("SeatVideosPlayerController.OnModeChange: " + mode);
            if (mode == AppModes.SeatVideos) 
			{
				StartLoop();
			}
			else
			{
				videoPlayer.DisplayVideo(false);
				videoBackground.SetActive(false);
                logoContainer.SetActive(true);
            }
		}

		public void StartLoop()
		{
            if (!ServerDataController.hasData && ServerDataController.overlayData.playlist.videos.Length == 0)
            {
                if (OnFinishedLoop != null) OnFinishedLoop.Invoke();
                ModesController.Instance.SetMode(AppModes.Standby, true);
                return;
            }

            currentVideoIndex = 0;
			videoBackground.SetActive(true);
			StartCoroutine(PlayCompanyVideo(currentVideoIndex));
            logoContainer.SetActive(false);
		}
		
		IEnumerator PlayCompanyVideo(int index)
		{
			yield return new WaitForSeconds(videoDelay);
			videoPlayer.DisplayVideo(true);
			//videoPlayer.PlayVideo("file://" + videoFiles[currentVideoIndex]);
			videoPlayer.PlayVideo(ServerDataController.overlayData.playlist.videos[currentVideoIndex].video.file);

        }

		void OnVideoFinish()
		{
			if(modesController.GetCurrentMode() == AppModes.SeatVideos)
			{
                Debug.Log("VIDEO FINSIHED = " + currentVideoIndex + " // TOTAL: " + ServerDataController.overlayData.playlist.videos.Length + " // " + ServerDataController.overlayData.schedule.interactive);
				currentVideoIndex++;
				if(currentVideoIndex >= ServerDataController.overlayData.playlist.videos.Length) 
				{
                    Debug.Log("VIDEO LOOP FINSIHED");
                    currentVideoIndex = 0;
                    if (ServerDataController.hasData && ServerDataController.overlayData.schedule.cicle)
                    {
                        Debug.Log("VIDEO LOOP CONTINUE");
                        StartCoroutine(PlayCompanyVideo(currentVideoIndex));
                    } else
                    {
                        Debug.Log("VIDEO STANDBY");
                        ModesController.Instance.SetMode(AppModes.Standby, true);
                    }
                }
				else
				{
                    if (ServerDataController.hasData && ServerDataController.overlayData.schedule.interactive)
                    {
                        currentVideoIndex = 0;
                        ModesController.Instance.SetMode(AppModes.Standby, true);
                    } else { 
                        StartCoroutine(PlayCompanyVideo(currentVideoIndex));
                    }
                }
			}
		}

        void OnVideoError()
        {
            Debug.Log("VIDEO ERROR = " + currentVideoIndex + " // " + ServerDataController.overlayData.playlist.videos[currentVideoIndex].video.file);
            if (modesController.GetCurrentMode() == AppModes.SeatVideos)
            {
                currentVideoIndex = 0;
                ModesController.Instance.SetMode(AppModes.Standby, true);
            }
        }

    }
	
}

