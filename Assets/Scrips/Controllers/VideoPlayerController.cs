﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace CasaSeat
{
	public class VideoPlayerController : MonoBehaviour
	{
		public event Action videoFinished;
        public event Action videoError;

        private static VideoPlayerController _instance;
		public static VideoPlayerController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<VideoPlayerController>();

					if (_instance == null)
					{
						GameObject container = new GameObject("VIDEO PLAYER CONTROLLER");
						_instance = container.AddComponent<VideoPlayerController>();
					}
				}

				return _instance;
			}
		}
		
		private VideoPlayer videoPlayer;

        private void Awake()
        {
			videoPlayer = this.gameObject.GetComponent<VideoPlayer>();
            videoPlayer.prepareCompleted += ReadyPlay;
            videoPlayer.loopPointReached += VideoFinished;
            videoPlayer.errorReceived += ErrorReceived;
        }

        private void ErrorReceived(VideoPlayer source, string message)
        {
            if (videoError != null) videoError.Invoke();
        }

        void Start()
		{
			StartCoroutine(InitializeVideoPlayer());
		}

		IEnumerator InitializeVideoPlayer()
		{
			yield return new WaitForEndOfFrame();
			DisplayVideo(false);
		}

		public void DisplayVideo(bool display)
		{
            if (!display && videoPlayer.isPlaying) videoPlayer.Stop();
			this.gameObject.SetActive(display);
		}

		public void PlayVideo(string file)
		{
			videoPlayer.targetTexture.DiscardContents();
			videoPlayer.targetTexture.Release();
			
			videoPlayer.url = file;
			videoPlayer.Prepare();
            videoPlayer.Play();
		}

		void ReadyPlay(VideoPlayer player)
		{
			if(player.url != null) player.Play();
		}

		void VideoFinished(VideoPlayer player)
		{
			if(videoFinished != null) videoFinished();
		}
	}
	
}