﻿using System;
using System.Collections;
using UnityEngine;
using KinectSceneManager;
using Strobotnik.GUA;

namespace CasaSeat
{
	public class ModesController : MonoBehaviour
	{
        public float standbyDelay;
		public event Action<AppModes> OnAppModeChange;

		private static ModesController _instance;
		public static ModesController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<ModesController>();

					if (_instance == null)
					{
						GameObject container = new GameObject("MODES CONTROLLER");
						_instance = container.AddComponent<ModesController>();
					}
				}

				return _instance;
			}
		}

		private AppManager appManager;
		private AppModes currentMode = AppModes.SeatVideos;
		private AppModes previousMode = AppModes.SeatVideos;
		private UsersController kinectController;

		void Start()
		{
			appManager = AppManager.Instance;
			kinectController = UsersController.instance;
			kinectController.OnHasActiveUser += OnKinectStateChange;
			kinectController.OnLostActiveUser += OnKinectStateChange;

            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            yield return new WaitForEndOfFrame();
            SetMode(AppModes.Standby, true);
        }

		void OnKinectStateChange()
		{
			bool userAvailable = kinectController.HasActiveUser;
            Debug.Log("OnKinectStateChange: " + userAvailable);

            if (userAvailable && (!ServerDataController.hasData  || ServerDataController.overlayData.schedule.shared || ServerDataController.overlayData.schedule.interactive)) 
			{
				StopAllCoroutines();
				SetMode(AppModes.Interaction, true);
                OSCController.SendMessage(OSCController.OSC_MSG.USER_START);
                
            }
			if (!userAvailable && currentMode == AppModes.Interaction) 
			{
				StopAllCoroutines();
				SetMode(AppModes.Standby, false);
                OSCController.SendMessage(OSCController.OSC_MSG.USER_END);
            }
        }


		public void SetMode(AppModes mode, bool restart)
		{
			previousMode = currentMode;
			currentMode = mode;

			Debug.Log("SETTING MODE TO: " + currentMode);
			AppModeChangeEvent(currentMode, restart);

        }

		IEnumerator PreStandbyDelay()
		{
			yield return new WaitForSeconds(standbyDelay);
			SetMode(AppModes.Standby, false);
		}

		public AppModes GetCurrentMode()
		{
			return currentMode;
		}

		public AppModes GetPreviousMode()
		{
			return previousMode;
		}

		void AppModeChangeEvent(AppModes mode, bool restart)
		{
			if(OnAppModeChange != null) OnAppModeChange(mode);
		}

		// TODO: REMOVE AFTER DELEVELOPEMENT
		void Update()
		{
            /*
			if(Input.GetKeyDown(KeyCode.Z)) SetMode(AppModes.PreStandby, true);
			else if(Input.GetKeyDown(KeyCode.X)) SetMode(AppModes.Standby, true);
			else if(Input.GetKeyDown(KeyCode.C)) SetMode(AppModes.Interaction, true);
			else if(Input.GetKeyDown(KeyCode.V)) SetMode(AppModes.ForceStandby, true);
			else if(Input.GetKeyDown(KeyCode.B)) SetMode(AppModes.SeatVideos, true);
            */
		}
	}

	public enum AppModes
	{
		PreStandby,
		Standby,
		Interaction,
		ForceStandby,
		SeatVideos
	}	
}
