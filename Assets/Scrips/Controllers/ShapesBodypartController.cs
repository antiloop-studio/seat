﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;
using CasaSeat;

public class ShapesBodypartController : MonoBehaviour
{
    public Transform[] splineControlTransforms;
    public Vector3 offset;
    public float radius;
    public float spacing;
    private float _minSize, _maxSize;
    public float minSize { 
        get {
            return _minSize;
        } 
        set {
            if(value != _minSize)
            {
                _minSize = value;
                size = Random.Range(_minSize, _maxSize);
            }
        } 
    }  
    public float maxSize { 
        get {
            return _maxSize;
        } 
        set{
            if(value != _maxSize)
            {
                _maxSize = value;
                size = Random.Range(_minSize, _maxSize);
            }
        }
    }

    public bool particleParamsChanged { get; set; }
    private float _particleBaseForce, _particleForceMult, _particleDistMult, _particleHeadingEase, _particleRandom;
    public float particleBaseForce 
    { 
        get { 
            return _particleBaseForce; 
        } 
        set {
            if(_particleBaseForce != value)
            {
                particleParamsChanged = true;
                _particleBaseForce = value;
            }
        }
    }
    public float particleForceMult 
    { 
        get { 
            return _particleForceMult; 
        } 
        set {
            if(_particleForceMult != value)
            {
                particleParamsChanged = true;
                _particleForceMult = value;
            }
        }
    }
    public float particleDistMult 
    { 
        get { 
            return _particleDistMult; 
        } 
        set {
            if(_particleDistMult != value)
            {
                particleParamsChanged = true;
                _particleDistMult = value;
            }
        }
    }
    public float particleHeadingEase 
    { 
        get { 
            return _particleHeadingEase; 
        } 
        set {
            if(_particleHeadingEase != value)
            {
                particleParamsChanged = true;
                _particleHeadingEase = value;
            }
        }
    }
    public float particleRandom 
    { 
        get { 
            return _particleRandom; 
        } 
        set {
            if(_particleRandom != value)
            {
                particleParamsChanged = true;
                _particleRandom = value;
            }
        }
    }

    public float size = 1;
    public GameObject[] prefabRefs;
    public bool hasAttractor;
    public AppearanceUpdater appearance;
    
    [HideInInspector]
    public SplineComputer spline;
    private SplinePoint[] splinePoints;
    
    private List<SplineItem> items = new List<SplineItem>();
    private bool hasCollider = false;
    private bool hasPhysics = false;

    void Start()
    {
        splinePoints = new SplinePoint[splineControlTransforms.Length];

        spline = gameObject.AddComponent<SplineComputer>() as SplineComputer;
        spline.sampleRate = 30;
        

        UpdateSplineInformation();
        StartCoroutine(InitSpline());
    }

    IEnumerator InitSpline()
    {
        yield return new WaitForSeconds(1);
        spline.updateMode = SplineComputer.UpdateMode.LateUpdate;
        UpdateParticles();
    }

    void Update()
    {
        UpdateSplineInformation();

        if (particleParamsChanged)
        {
            for (int i = 0; i < items.Count; i++)
            {
                items[i].UpdateFollowerParams(particleBaseForce, particleForceMult, particleDistMult, particleHeadingEase, particleRandom);
            }
            particleParamsChanged = false;
        }

        if (AppearanceController.Instance.lerpingAppearances) {
            UpdateParticles();
        }
    }

    void UpdateParticles()
    {
        if (!spline) return;

        int desiredAmount = Mathf.FloorToInt(spline.CalculateLength() / spacing);
        if (desiredAmount > items.Count)
        {
            // ADD MISSING SPLINE ITEMS
            for (int i = 0; i < desiredAmount - items.Count; i++)
            {
                int prefabId = GetPrefabType();
                if (prefabId >= 0)
                {
                    GameObject go = new GameObject();
                    go.transform.parent = transform;
                    go.name = "SPLINE ITEM";
                    SplineItem item = go.AddComponent<SplineItem>();


                    item.goRef = prefabRefs[GetPrefabType()];
                    item.hasCollider = hasCollider;
                    item.hasPhysycs = hasPhysics;

                    item.index = items.Count;
                    item.splineController = this;
                    items.Add(item);
                    item.Init();
                }
            }
        }
        else if (desiredAmount < items.Count)
        {
            // REMOVE SPLINE ITEMS
            for (int i = items.Count - 1; i >= desiredAmount; i--)
            {
                if (items[i]) items[i].Remove();
                items.RemoveAt(i);
            }
        }
    }

    public void ExplodeParticles()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].Explode();
        }
        
        items.Clear();
        
        //UpdateParticles();
    }

    public void SetGravity(bool val)
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].magnet.Gravity(val);
        }
    }


    public void EraseParticles()
    {
        for (int i = items.Count - 1; i >= 0; i--)
        {
            if (items[i]) items[i].Remove();
        }
        items.Clear();
        UpdateParticles();
    }

    int GetPrefabType()
    {
        int result = -1;
        float total = 0;
        int _length = (int)CHARACTER_MODIFIER.PARTICLE_TYPE_21 - (int)CHARACTER_MODIFIER.PARTICLE_TYPE_1 + 1;
        float[] weightRanges = new float[_length];
        for (int i = 0; i < _length; i++)
        {
            total += appearance.VisualData.originalValues[((int)CHARACTER_MODIFIER.PARTICLE_TYPE_1 + i)][0];
            weightRanges[i] = total;
        }
        if (total > 0) { 
            float _r = Random.Range(0, total);
            bool found = false;
            //Debug.Log(total + " // " + _r);
            for (int i = 0; i < weightRanges.Length; i++)
            {
                if (_r < weightRanges[i] && !found)
                {
                    result = i;
                    found = true;
                }
            }
        }

        return result;
    }

    public void SetCollider(bool val)
    {
        hasCollider = val;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].magnet) items[i].magnet.magnetCollider.enabled = hasCollider;
        }
    }

    public void SetPhysics(bool val)
    {
        hasPhysics = val;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].magnet && items[i].magnet.rb)
            {
                items[i].magnet.rb.angularVelocity = Vector3.zero;
                items[i].magnet.rb.velocity = Vector3.zero;
                items[i].magnet.hasPhysics = val;
            }
        }
    }

    void UpdateSplineInformation()
    {
        for (int i = 0; i < splineControlTransforms.Length; i++)
        {
            splinePoints[i].SetPosition(splineControlTransforms[i].position + offset);
        }
        spline.SetPoints(splinePoints, Dreamteck.Splines.SplineComputer.Space.World);
    }


    public void UpdateParticleParams(float bf, float fm, float dm, float he, float pr)
    {
        particleBaseForce = bf;
        particleForceMult = fm;
        particleDistMult = dm;
        particleHeadingEase = he;
        particleRandom = pr;
    }
}

public enum SizeStyle
{
    Uniform,
    StartLargeEndSmall,
    StartSmallEndLarge,
    StartSmallMidLargeEndSmall,
    StartLargeMidSmallEndLarge
}
