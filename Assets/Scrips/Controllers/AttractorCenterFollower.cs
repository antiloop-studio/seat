﻿using UnityEngine;

public class AttractorCenterFollower : MonoBehaviour
{
    
    public ShapesBodypartController splineController = null;
    public float baseForce = 100;
    public float forceMult = 1;
    public float distantMult = 1;
    public float headingEase = 0.1f;

    public Transform target;
    [HideInInspector] public Vector3 targetPosition;
    public Rigidbody followObject;
    public float sizeMultiplier = 1;
    public bool isVisible = true;

    
    public Transform particleTransform;
    private float speed, distance;
    private Vector3 heading;

    private bool isFixed = true;
    // Start is called before the first frame update
    void Start()
    {
        // target = transform;
        if (!particleTransform) {
            GameObject g =  GameObject.Instantiate(followObject.gameObject, transform.position, Quaternion.identity, transform);
            followObject = g.GetComponent<Rigidbody>() as Rigidbody;
            particleTransform = g.transform;
            if(!isVisible)
            {
                g.GetComponentInChildren<MeshRenderer>().enabled = false;
            }
        }
        if (target) particleTransform.position = target.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        ApplyForce();
    }

    void ApplyForce() {
        if (target) targetPosition = target.position;
        
        distance = Vector3.Distance(targetPosition, particleTransform.position);
        speed = followObject.velocity.magnitude;
        heading = targetPosition - particleTransform.position;

        followObject.velocity += (heading - followObject.velocity) * headingEase;

        float s = (baseForce + (distantMult * distance * distance));
        followObject.AddForce((heading * s * forceMult));
        
        //followObject.transform.position = targetPosition;

        if (splineController) {
            particleTransform.transform.localScale = Vector3.one * splineController.size;
        }
    }
}
