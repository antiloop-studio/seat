﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
	public class CameraController : MonoBehaviour
	{
        public static CameraController instance;
		public float cameraPadding, moveTime;
		public LeanTweenType cameraEasing;
		public int cameraStandbyLocationIndex;
		public Transform characterGroundOrigin, characterRotationOrigin;
		public CameraPoint[] cameraLocations;

		public Transform[] centerBoundsTransforms;
		public Transform[] totalBoundsTransforms;

		private StatesController statesController;
		private ModesController modesController;

		private Camera mainCamera;
		private Bounds characterCenter, characterTotal;
        private Vector3[] centerPoints, totalPoints;
        public Transform lookAtCenter;
		
		private bool isLerping = false;
		private int currentCameraPointIndex; //bit overkill, but hey 

		void Start()
		{
            instance = this;
            mainCamera = Camera.main;

			modesController = ModesController.Instance;
            statesController = StatesController.Instance;
            
            totalPoints = new Vector3[totalBoundsTransforms.Length];
            centerPoints = new Vector3[centerBoundsTransforms.Length];

			StartCoroutine(MoveToNewPoint(3, 1));
            UpdateCamera();
            Invoke("UpdateCamera", 1);
		}

		IEnumerator MoveToNewPoint(float delay, int index)
		{
			yield return new WaitForSeconds(delay);
            if (!isLerping) SetNewCameraPositionIndex(index);
		}

		public void UpdateCamera()
		{
			UpdatePoints(centerBoundsTransforms, centerPoints);
            UpdatePoints(totalBoundsTransforms, totalPoints);

            lookAtCenter.position = UpdateBounds(centerPoints).center * 1.25f;

            //UpdateCameraTransform();

            //mainCamera.transform.LookAt(lookAtCenter);
        }

		void UpdatePoints(Transform[] transforms, Vector3[] points)
        {
            for (int i = 0; i < transforms.Length; i++)
            {
                points[i] = Vector3.zero;
                points[i].y = transforms[i].position.y;
            }
        }

		Bounds UpdateBounds(params Vector3[] points)
        {
            return GeometryUtility.CalculateBounds(points, Matrix4x4.identity);
        }

		void UpdateCameraTransform()
		{
			transform.position = characterGroundOrigin.position;
			transform.eulerAngles = new Vector3(0, characterRotationOrigin.rotation.eulerAngles.y, 0);

			Vector3 characterSize = UpdateBounds(totalPoints).size;
            float objectSize = Mathf.Max(Mathf.Max(characterSize.x / 2, characterSize.y / 2), characterSize.z / 2);
            float cameraView = 2f * Mathf.Tan(0.5f * Mathf.Deg2Rad * Camera.main.fieldOfView);
            float rigRadius = cameraPadding * objectSize / cameraView;

            Vector3 pos = mainCamera.transform.localPosition;
            pos.z = rigRadius;
            mainCamera.transform.localPosition = pos;
		}

		void SetNewCameraPositionIndex(int index)
		{
			if(index >= cameraLocations.Length) throw new System.Exception("Index is out of bounds");
			if(index == currentCameraPointIndex) throw new System.Exception("Same as current index");
			
			currentCameraPointIndex = index;
			
			isLerping = true;

		}
	}

	[System.Serializable]
	public class CameraPoint
	{
		[Range(0,1)]
		public float heightPercentage;
		[Range(-40, 40)]
		public float offsetAngle;

		public static CameraPoint Lerp(CameraPoint a, CameraPoint b, float t)
		{
			CameraPoint output = new CameraPoint();
			output.heightPercentage = Mathf.Lerp(a.heightPercentage, b.heightPercentage, t);
			output.offsetAngle = Mathf.Lerp(a.offsetAngle, b.offsetAngle, t);

			return output;
		}
	}
	
}