﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorFollowers : MonoBehaviour
{
    public float baseForce = 100;
    [Range(0,1)]
    public float baseForceRandom;
    public float forceMult = 1;
    [Range(0,1)]
    public float forceMultRandom;
    public float distantMult = 1;
    [Range(0,1)]
    public float distantMultRandom;
    public float headingEase = 0.1f;
    [Range(0,1)]
    public float headingEaseRandom;

    public Transform target;

    public int followObjectsMultiplier = 1;
    public Rigidbody[] followObjects;

    private FollowParticle[] particles;

    void Start()
    {
        particles = new FollowParticle[followObjects.Length * followObjectsMultiplier];

        int followObjectIndex = 0;
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i] = new FollowParticle(
                followObjects[followObjectIndex].gameObject, 
                transform,
                headingEase + Random.Range(-headingEase * headingEaseRandom, headingEase * headingEaseRandom),
                forceMult + Random.Range(-forceMult * forceMultRandom, forceMult * forceMultRandom),
                distantMult + Random.Range(-distantMult * distantMultRandom, distantMult * distantMultRandom),
                baseForce + Random.Range(-baseForce * baseForceRandom, baseForce * baseForceRandom)
            );

            if(i != 0 )
            {
                if(i % followObjectsMultiplier == 0) followObjectIndex++;
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].UpdateParticle(target);
        }
    }

    public void SetSizes(float min, float max)
    {
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].SetSize(Random.Range(min, max));
        }
    }
}


public class FollowParticle
{
    private Rigidbody particlePhysics;
    private Transform particleTransform;

    private float speed, distance;
    private float headingEase, forceMult, distantMult, baseForce;
    private Vector3 heading;

    public FollowParticle(GameObject particle, Transform parent, float headingEase, float forceMult, float distantMult, float baseForce)
    {
        GameObject p =  GameObject.Instantiate(particle, Random.insideUnitSphere * 2, Quaternion.identity, parent);
        particlePhysics = p.GetComponent<Rigidbody>() as Rigidbody;
        particleTransform = p.transform;

        this.forceMult = forceMult;
        this.headingEase = headingEase;
        this.distantMult = distantMult;
        this.baseForce = baseForce;
    }

    public void UpdateParticle(Transform target)
    {
        distance = Vector3.Distance(target.position, particleTransform.position);
        speed = particlePhysics.velocity.magnitude;
        heading = target.position - particleTransform.position;

        particlePhysics.velocity += (heading - particlePhysics.velocity) * headingEase * forceMult;

        float s = (baseForce + (distantMult * distance * distance));
        particlePhysics.AddForce((heading * s * forceMult));
    }

    public void SetSize(float size)
    {
        if (particleTransform && particleTransform.GetChild(0) != null) particleTransform.GetChild(0).localScale *= size;
    }
}
