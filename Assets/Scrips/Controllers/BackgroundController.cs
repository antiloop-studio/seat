﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

namespace CasaSeat
{

    public class BackgroundController : MonoBehaviour
    {
        public Camera mainCamera;
        public Image imgBgBack1;
        public Image imgBgBack2;
        public Image imgBgFloor;
        public Image imgBgFront;
        public MeshRenderer floorBase;
        public Volume bg;
		public Color32 buildUnbuildColor;
		public OverlayColor[] overlayColors;

        private float screenHeight = 1024;

        private static BackgroundController _instance;
        public static BackgroundController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<BackgroundController>();

                    if (_instance == null)
                    {
                        GameObject container = new GameObject("BACKGROUND CONTROLLER");
                        _instance = container.AddComponent<BackgroundController>();
                    }
                }

                return _instance;
            }
        }

        private GraphicsOverlayController graphicsController;
        private StatesController statesController;
		private Dictionary<GraphicOverlays, Color32> colorsDict;
		private HDAdditionalCameraData cameraBackground;

        void Start()
        {
            graphicsController = GraphicsOverlayController.Instance;
            statesController = StatesController.Instance;
            graphicsController.OnGraphicOverlayChange += OnGraphicOverlayChange;
            statesController.OnAppStateChange += OnStateChange;

			cameraBackground = mainCamera.GetComponent<HDAdditionalCameraData>();
			colorsDict = new Dictionary<GraphicOverlays, Color32>();
            
            foreach (OverlayColor overlayColor in overlayColors)
			{
				colorsDict.Add(overlayColor.overlay, overlayColor.color);
			}

            imgBgFront.rectTransform.anchoredPosition = new Vector2(0, Screen.height);
        }

        void OnStateChange(AppStates state)
        {
            //if(state == AppStates.Build || state == AppStates.Unbuild) SetBackground(buildUnbuildColor);
        }

        void OnGraphicOverlayChange(GraphicOverlays overlay)
        {
            //Debug.Log("BackgroundController.OnGraphicOverlayChange = " + statesController.GetCurrentState() + " // " + overlay);
            Color32 color = Color.black;

            if (colorsDict.TryGetValue(overlay, out color))
            {
                SetBackground(color);
            }
            else throw new System.Exception("NO COLLOR SPECIFIED FOR OVERLAY");
        }

        void SetBackground(Color c)
        {
            OSCController.SendMessage(OSCController.OSC_MSG.BACKGROUND_CHANGE);

            LeanTween.cancel(imgBgBack2.rectTransform);
            LeanTween.cancel(imgBgFront.rectTransform);
            imgBgBack2.rectTransform.anchoredPosition = new Vector2(0, screenHeight);
            imgBgFront.rectTransform.anchoredPosition = new Vector2(0, screenHeight);

            imgBgBack2.color = c;
            imgBgFront.color = c;

            RectTransform bgToMove = imgBgBack2.rectTransform;
            if (statesController.GetCurrentState() == AppStates.Hello)
            {
                bgToMove = imgBgFront.rectTransform;
            }

            LeanTween.move(bgToMove, new Vector2(0, 0), 1.2f).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() => {
                imgBgBack1.color = imgBgBack2.color;
                imgBgFloor.color = imgBgBack1.color;
                if (statesController.GetCurrentState() == AppStates.Hello)
                {
                    imgBgBack1.color = colorsDict[GraphicOverlays.NoOverlay];
                    imgBgFloor.color = imgBgBack1.color;
                    LeanTween.move(imgBgFront.rectTransform, new Vector2(0, -screenHeight), 1.2f).setEase(LeanTweenType.easeInOutQuad).setDelay(2f);
                }
            });

            
        }
    }

	[System.Serializable]
	public struct OverlayColor
	{
		public GraphicOverlays overlay;
		public Color32 color;
	}
}
