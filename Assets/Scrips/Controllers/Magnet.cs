﻿using UnityEngine;

public class Magnet : MonoBehaviour
{
    private Collider _magnetCollider;
    public Collider magnetCollider
    {
        get
        {
            if (_magnetCollider == null) _magnetCollider = gameObject.GetComponent<Collider>();
            return _magnetCollider;
        }
    }
    public bool hasPhysics = true;

    public ShapesBodypartController splineController = null;
    public float baseForce = 100;
    public float forceMult = 1;
    public float distantMult = 1;
    public float headingEase = 0.1f;

    public Transform target;
    [HideInInspector] public Vector3 targetPosition;
    public Rigidbody rb;
    public float sizeMultiplier = 1;
    public bool isVisible = true;
    public bool isMagnet = true;
    private float speed, distance;
    private Vector3 heading;

    private bool isFixed = true;
    // Start is called before the first frame update
    void Start()
    {
       rb = gameObject.GetComponent<Rigidbody>();
       transform.position = target.position;
       transform.eulerAngles = new Vector3(Random.Range(3, 360), Random.Range(3, 360), Random.Range(3, 360));
    }

    public void Explode()
    {
        if (!gameObject || !rb) return;
        isMagnet = false;
        rb.gameObject.layer = 14;
        magnetCollider.enabled = true;
        rb.useGravity = true;
        rb.velocity = Vector3.zero;
        rb.AddForce(Random.onUnitSphere * Random.Range(0.2f, 0.4f), ForceMode.Impulse);
    }

    public void Gravity(bool val)
    {
        if (!gameObject || !rb) return;
        isMagnet = false;
        rb.gameObject.layer = 14;
        magnetCollider.enabled = true;
        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        rb.AddForce(Random.onUnitSphere * Random.Range(0f, 1.1f), ForceMode.Impulse);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!isMagnet) return;

        if (hasPhysics) { 
            ApplyForce();
        } else
        {
            if (target) transform.position = target.position;
        }
    }

    void ApplyForce() {
        if (target) targetPosition = target.position;
        
        distance = Vector3.Distance(targetPosition, transform.position);
        speed = rb.velocity.magnitude;
        heading = targetPosition - transform.position;

        rb.velocity += (heading - rb.velocity) * headingEase;

        float s = (baseForce + (distantMult * distance * distance));
        rb.AddForce((heading * s * forceMult));
        
        //followObject.transform.position = targetPosition;

        if (splineController) {
            transform.transform.localScale = Vector3.one * splineController.size;
        }
    }
}
