using UnityEngine;

namespace CasaSeat
{
	[System.Serializable]
    public class ScaleBodyPart
    {
        public CHARACTER_MODIFIER bodyPartName;
		public ScaleInfo[] scaleInfos;
        public Transform[] originJoint;
        public Transform[] modifyJoint;
    
        private Vector3[] originalModifyJointPositions;

        public void InitScaleBodyPart()
        {
            originalModifyJointPositions = new Vector3[modifyJoint.Length];

            for (int i = 0; i < originalModifyJointPositions.Length; i++)
            {
                originalModifyJointPositions[i] = modifyJoint[i].localPosition;
            }
        }

        public void UpdateVisibleBodyParts(int index)
        {
            float scaleFactor = Vector3.Distance(originJoint[index].position, modifyJoint[index].position);
        }

		public void UpdateScale()
        {
			Vector3 scaleValue = new Vector3();
			for (int i = 0; i < modifyJoint.Length; i++)
			{
				foreach (ScaleInfo scale in scaleInfos)
				{
					switch (scale.scaleType)
					{
						case ScaleType.MoveY:
							scaleValue.y = scale.scaleAmount;
							break;
						case ScaleType.MoveX:
							scaleValue.x = scale.scaleAmount;
							break;
						case ScaleType.ScaleUniform:
							scaleValue = new Vector3(scale.scaleAmount, scale.scaleAmount, scale.scaleAmount);
							break;
						default: throw new System.Exception("This scale is not possible");
					}
				}

				UpdateJointsScale(i, scaleValue);
				UpdateVisibleBodyParts(i); 	
			}
        }

        void UpdateJointsScale(int index, Vector3 scale)
        {
            modifyJoint[index].localPosition = Vector3.Scale(originalModifyJointPositions[index], scale);
		}
    }

	[System.Serializable]
    public class AvatarSkeleton
    {
        public Transform root;
        [HideInInspector]
        public Transform[] joints;

        public void InitAvatarSkeleton()
        {
            Transform baseJoint = root.GetChild(2);
            joints = baseJoint.GetComponentsInChildren<Transform>();
        }
    }

	[System.Serializable]
	public struct ScaleInfo
	{
		public ScaleType scaleType;
		public float scaleAmount;
	}

	[System.Flags]
    public enum ScaleType
    {
        MoveY,
        MoveX,
        ScaleUniform
    }

    public enum BodyPartNames
    {
        Neck,
        Shoulders,
        Torso,
        UpperArms,
        LowerArms,
        Hips,
        UpperLegs,
        LowerLegs
    }
}
