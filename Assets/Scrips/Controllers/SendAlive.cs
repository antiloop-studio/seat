﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class SendAlive : MonoBehaviour
{

    // udpclient object
    UdpClient client;

    // port number
    public int port = 9900;
    public string address = "10.42.60.1";
    public FPSCounter fps;

    private bool doAlive = true;
    // Start is called before the first frame update
    void Start()
    {
        client = new UdpClient();
        IPEndPoint hostEndPoint = new IPEndPoint(IPAddress.Parse(address), port);
        client.Connect(hostEndPoint);
        client.Client.Blocking = false;

        InvokeRepeating("SendAliveMessage", 1, 5);
    }

    public void SetAlive(bool val)
    {
        doAlive = val;
    }

    void SendAliveMessage()
    {
        if (fps.m_CurrentFps < 20)
        {
            Debug.Log("NO ALIVE MESSAGE DUE TO LOW FPS");
            return;
        }
        if (!doAlive) return;

        Debug.Log("SEND ALIVE");
        // sending data
        byte[] message = Encoding.UTF8.GetBytes("1");
        client.Send(message, message.Length);
    }

    private void OnApplicationQuit()
    {
        client.Close();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
