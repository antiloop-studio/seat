﻿using BestHTTP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class UserStats : MonoBehaviour
{
    public static UserStats instance;

    public bool isTest;
    public string url;
    public string urlTest;

    private StatObject stat;
    private List<int> statLeftHand = new List<int>();
    private List<int> statRightHand = new List<int>();
    private List<int> statBothHands = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        stat = new StatObject();
    }

    // Update is called once per frame
    void Update()
    {
        //if (!isTest) return;

        if (Input.GetKeyDown(KeyCode.S))
        {
            
            StartSession();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("STAT: LEFT HAND");
            HandInteraction(0);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("STAT: RIGHT HAND");
            HandInteraction(1);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log("STAT: BOTH HANDS");
            HandInteraction(2);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            
            EndSession();
            //Post(isTest ? urlTest : url, jsonString);
        }
    }

    public static void StartSession()
    {
        Debug.Log("STAT: START SESSION");
        instance.stat = new StatObject();
        instance.stat.user_in = CurrentTime();

        instance.statLeftHand.Clear();
        instance.statRightHand.Clear();
        instance.statBothHands.Clear();
    }

    public static void HandInteraction(int id)
    {
        if (id == 0) instance.statLeftHand.Add(CurrentTime());
        if (id == 1) instance.statRightHand.Add(CurrentTime());
        if (id == 2) instance.statBothHands.Add(CurrentTime());
    }

    public static void UserFinished()
    {
        instance.stat.user_has_finished = true;
    }

    public static void EndSession()
    {
        Debug.Log("STAT: END SESSION");
        instance.stat.user_out = CurrentTime();

        instance.stat.left_hand = instance.statLeftHand.ToArray();
        instance.stat.right_hand = instance.statRightHand.ToArray();
        instance.stat.both_hands = instance.statBothHands.ToArray();

        string jsonString = JsonUtility.ToJson(instance.stat);
instance.Post(jsonString);
    }

    void Post(string bodyJsonString)
    {
        Debug.Log("STAT: POST - " + bodyJsonString);
        
        HTTPRequest request = new HTTPRequest(new Uri(isTest ? urlTest : url), HTTPMethods.Post, OnRequestFinished);
        Encoding.UTF8.GetBytes(bodyJsonString);
        request.AddHeader("Content-Type", "application/json");
        request.RawData = Encoding.UTF8.GetBytes(bodyJsonString);
        request.Send();

    }

    private void OnRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
        Debug.Log("STAT RESPONSE: " + response.DataAsText);
    }

    public static int CurrentTime()
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;

        return currentEpochTime;
    }
}

public struct StatObject
{
    public int user_in;
    public int[] left_hand;
    public int[] right_hand;
    public int[] both_hands;
    public int user_out;
    public bool user_has_finished;
}