﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
    public class AppManager : MonoBehaviour
    {
        private static AppManager _instance;
        public static AppManager Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<AppManager>();
                    
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("APP MANAGER");
                        _instance = container.AddComponent<AppManager>();
                    }
                }
            
                return _instance;
            }
        }
        public static bool isAlex = false;

        public GameObject[] debugContainer;
        public RectTransform uiCameraImage;
        public RenderTexture cameraRenderTexture;
        public RectTransform canvasOverlay;
        public InputField inputScreenX;
        public InputField inputScreenY;
		[HideInInspector]
		public bool controllAppFromServer = true;

        private bool isDebug = false;
		private ServerDataController serverDataController;
        private GraphicsOverlayController overlayController;

		private AppBehaviour currentAppBehaviour, previousAppBehaviour, upcomingAppBehaviour = AppBehaviour.Character;
		private bool appLaunch = true;
        private bool isLedOutput;

        void Start()
        {
            LeanTween.init(2000);
            LeanTween.cancelAll();
            foreach (var item in debugContainer)
            {
                item.SetActive(isDebug);
            }
            isDebug = false;

            SetLedOutput(true);
            StartCoroutine(InitialzeApp());

            if (PlayerPrefs.HasKey("screen-res-x")) inputScreenX.text = PlayerPrefs.GetInt("screen-res-x") + "";
            else inputScreenX.text = "1024";
            if (PlayerPrefs.HasKey("screen-res-y")) inputScreenY.text = PlayerPrefs.GetInt("screen-res-y") + "";
            else inputScreenY.text = "512";
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.P)) {
                isDebug = !isDebug;
                foreach (var item in debugContainer)
                {
                    item.SetActive(isDebug);
                }
                
            }
            if (Input.GetKeyDown(KeyCode.O)) {
                SetLedOutput(!isLedOutput);
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (Screen.fullScreen) Screen.fullScreenMode = FullScreenMode.Windowed;
                else Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
            }
        }

        IEnumerator InitialzeApp()
        {
            yield return new WaitForEndOfFrame();

			serverDataController = ServerDataController.Instance;
			serverDataController.UpdateData();
			serverDataController.serverDataUpdated += OnServerData;
        }

        void SetLedOutput(bool val)
        {
            isLedOutput = val;
            if (isLedOutput)
            {
                Camera.main.targetTexture = cameraRenderTexture;
                uiCameraImage.gameObject.SetActive(true);
                canvasOverlay.localScale = new Vector3(1, 0.5f, 1);
            }
            else
            {
                Camera.main.targetTexture = null;
                uiCameraImage.gameObject.SetActive(false);
                canvasOverlay.localScale = Vector3.one;
            }
        }

		void OnServerData(DomesticApiData res)
		{
			var appbehaviour = res.schedule;

			if(appbehaviour.cicle) upcomingAppBehaviour = AppBehaviour.Videos;
			else if(appbehaviour.interactive) upcomingAppBehaviour = AppBehaviour.Character;
			else if(appbehaviour.shared) upcomingAppBehaviour = AppBehaviour.Shared;

            if (appbehaviour.cicle && ModesController.Instance.GetCurrentMode() != AppModes.SeatVideos)
            {
                ModesController.Instance.SetMode(AppModes.SeatVideos, true);
            }
		}


        public void SetAlex(bool val)
        {
            isAlex = val;
        }

        public void SetScreenWidth(string _input)
        {
            int _x = 0;
            int.TryParse(_input, out _x);
            canvasOverlay.sizeDelta = new Vector2(_x, canvasOverlay.sizeDelta.y);
            uiCameraImage.sizeDelta = new Vector2(_x, uiCameraImage.sizeDelta.y);
            PlayerPrefs.SetInt("screen-res-x", _x);
        }

        public void SetScreenHeight(string _input)
        {
            int _y = 0;
            int.TryParse(_input, out _y);
            canvasOverlay.sizeDelta = new Vector2(canvasOverlay.sizeDelta.x, _y * 2);
            uiCameraImage.sizeDelta = new Vector2(canvasOverlay.sizeDelta.x, _y);
            PlayerPrefs.SetInt("screen-res-y", _y);
        }
    }

	public enum AppBehaviour
	{
		None,
		Shared,
		Character,
		Videos
	}

}
