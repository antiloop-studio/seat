﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
    public class SimulateStateData : MonoBehaviour
    {
        public SimulateStates state;
    }

    public enum SimulateStates
    {
        Interaction,
        ForceStandby,
        PreStandby,
        Standby,
		SeatVideos,
        Introduction,
        Weather,
        Traffic,
        Events,
        Leave
    }   
}