﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
    public enum CHARACTER_MODIFIER {
        HEAD_OFFSET,
        NECK_OFFSET,
        SHOULDERS_X,
        SHOULDERS_Y,
        UPPER_ARMS,
        LOWER_ARMS,
        TORSO,
        WAIST_WIDTH,
        WAIST_HEIGHT,
        HIPS,
        UPPER_LEGS,
        LOWER_LEGS,

        HEAD_RADIUS,
        NECK_TOP_RADIUS,
        NECK_BOTTOM_RADIUS,
        SHOULDER_SPHERE_RADIUS,
        ARMS_TOP_RADIUS,
        ARMS_MIDDLE_RADIUS,
        ARMS_BOTTOM_RADIUS,
        TORSO_UP,
        TORSO_MIDDLE,
        TORSO_DOWN,
        HIP_SPHERE_RADIUS,
        LEGS_TOP_RADIUS,
        LEGS_MID_RADIUS,
        LEGS_BOTTOM_RADIUS,

        PARTICLE_SIZE,
        PARTICLE_SPACING,
        PARTICLE_OFFSET,
        PARTICLE_BASE_FORCE,
        PARTICLE_FORCE_MULT,
        PARTICLE_DIST_MULT,
        PARTICLE_HEADING_EASE,

        PARTICLE_TYPE_1,
        PARTICLE_TYPE_2,
        PARTICLE_TYPE_3,
        PARTICLE_TYPE_4,
        PARTICLE_TYPE_5,
        PARTICLE_TYPE_6,
        PARTICLE_TYPE_7,
        PARTICLE_TYPE_8,
        PARTICLE_TYPE_9,
        PARTICLE_TYPE_10,
        PARTICLE_TYPE_11,
        PARTICLE_TYPE_12,
        PARTICLE_TYPE_13,
        PARTICLE_TYPE_14,
        PARTICLE_TYPE_15,
        PARTICLE_TYPE_16,
        PARTICLE_TYPE_17,
        PARTICLE_TYPE_18,
        PARTICLE_TYPE_19,
        PARTICLE_TYPE_20,
        PARTICLE_TYPE_21,

        NECK_RING_TOP_HEIGHT,
        NECK_RING_TOP_RADIUS,
        NECK_RING_TOP_CURVE_RADIUS,
        NECK_RING_BOTTOM_HEIGHT,
        NECK_RING_BOTTOM_RADIUS,
        NECK_RING_BOTTOM_CURVE_RADIUS,
        Length
    }

    public class AppearanceModifier : MonoBehaviour
    {
        public static bool isSliderUpdate = true;

        public Button saveAppearanceButton;

        public RangeSlider[] sliders;
        public Dictionary<CHARACTER_MODIFIER, RangeSlider> slidersDict = new Dictionary<CHARACTER_MODIFIER, RangeSlider>();
        public Toggle particleHasCollider;
        public Toggle particleHasPhysics;

        public Toggle toggleTopRing;
        public Toggle toggleBottomRing;


        private AppearanceController appearanceController;

        private AppearanceUpdater currentAppearance;

        void Start()
        {
            appearanceController = AppearanceController.Instance;

            appearanceController.appearanceUpdaterChanged += OnAppearanceChanged;
            appearanceController.UpdateUI += UpdateUI;

            saveAppearanceButton.onClick.AddListener(delegate { appearanceController.SaveAppearances(); });
            
            currentAppearance = appearanceController.GetCurrentAppearanceUpdater();

            foreach (var item in sliders)
            {

                slidersDict.Add(item.sliderType, item);
                int id = (int)item.sliderType;
                item.SetValues(currentAppearance.VisualData.originalValues[id][0], currentAppearance.VisualData.originalValues[id][1]);
                item.OnChanged += OnSliderValChanged;
            }

            particleHasCollider.onValueChanged.AddListener(ParticleColliderValueChanged);
            particleHasPhysics.onValueChanged.AddListener(ParticlePhysicsValueChanged);

            toggleTopRing.onValueChanged.AddListener(ToggleTopRingChanged);
            toggleBottomRing.onValueChanged.AddListener(ToggleBottomRingChanged);

            appearanceController.SetParticlePhysicsChanged(currentAppearance);
            appearanceController.SetParticleColliderChanged(currentAppearance);
            appearanceController.SetParticleTypeChanged(currentAppearance);
        }

        private void Update()
        {
            
        }

        private void ToggleBottomRingChanged(bool val)
        {
            if (isSliderUpdate)
            {
                currentAppearance.VisualData.hasBottomRing = val;
                appearanceController.SetRingChanged(currentAppearance);
            }
        }

        private void ToggleTopRingChanged(bool val)
        {
            if (isSliderUpdate)
            {
                currentAppearance.VisualData.hasTopRing = val;
                appearanceController.SetRingChanged(currentAppearance);
            }
        }

        private void OnSliderValChanged(RangeSlider _slider)
        {
            currentAppearance = appearanceController.GetCurrentAppearanceUpdater();
            if (isSliderUpdate)
            {
                Debug.Log("SLIDER = " + _slider.sliderType);
                int _id = (int)_slider.sliderType;
                currentAppearance.VisualData.originalValues[_id][0] = _slider.slider1.value;
                if (_slider.isRange)
                {
                    currentAppearance.VisualData.originalValues[_id][1] = _slider.slider2.value;
                    currentAppearance.VisualData.processedValues[_id] = UnityEngine.Random.Range(_slider.slider1.value, _slider.slider2.value);
                } else
                {
                    currentAppearance.VisualData.originalValues[_id][1] = _slider.slider1.value;
                    currentAppearance.VisualData.processedValues[_id] = _slider.slider1.value;
                }

                if (_id >= (int)CHARACTER_MODIFIER.PARTICLE_TYPE_1 && _id <= (int)CHARACTER_MODIFIER.PARTICLE_TYPE_21)
                {
                    appearanceController.SetParticleTypeChanged(currentAppearance);
                }
                StopAllCoroutines();
                appearanceController.lerpingAppearances = true;
                StartCoroutine(CRSetLerpingOff());
            }
        }

        IEnumerator CRSetLerpingOff()
        {
            yield return new WaitForEndOfFrame();
            appearanceController.lerpingAppearances = false;
        }


        private void ParticlePhysicsValueChanged(bool val)
        {
            if (isSliderUpdate)
            {
                currentAppearance.VisualData.particleHasPhysics = val;
                appearanceController.SetParticlePhysicsChanged(currentAppearance);
            }
        }

        private void ParticleColliderValueChanged(bool val)
        {
            if (isSliderUpdate)
            {
                currentAppearance.VisualData.particleHasCollider = val;
                appearanceController.SetParticleColliderChanged(currentAppearance);
            }
        }


        void UpdateUI(AppearanceUpdater _appearance)
        {
            isSliderUpdate = false;

            particleHasCollider.isOn = _appearance.VisualData.particleHasCollider;
            particleHasPhysics.isOn = _appearance.VisualData.particleHasPhysics;

            toggleTopRing.isOn = _appearance.VisualData.hasTopRing;
            toggleBottomRing.isOn = _appearance.VisualData.hasBottomRing;

            foreach (var item in sliders)
            {
                int _id = (int)item.sliderType;
                item.SetValues(_appearance.VisualData.originalValues[_id][0], _appearance.VisualData.originalValues[_id][1]);
            }

            isSliderUpdate = true;
        }

        void OnAppearanceChanged(AppearanceUpdater appearance)
        {
            currentAppearance = appearance;
        }
    }

	[System.Serializable]
	public class SlidersContainer
	{
		public List<Slider> sliders = new List<Slider>();
	}
}


