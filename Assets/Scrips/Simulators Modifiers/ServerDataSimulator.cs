﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
    public class ServerDataSimulator : MonoBehaviour
    {
        public static ServerDataSimulator _instance;
        public static ServerDataSimulator Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<ServerDataSimulator>();
                    
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("SERVER DATA SIMULATOR");
                        _instance = container.AddComponent<ServerDataSimulator>();
                    }
                }
            
                return _instance;
            }
        }
        
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }
   
}