﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
    public class AppSimulator : MonoBehaviour
    {
		public Toggle useServerData;
        public List<Button> SimulateButtons;
        public Text currentMode, currentState;
        
		private AppManager appManager;
        private StatesController statesController;
        private ModesController modesController;

        void Start()
        {
			appManager = AppManager.Instance;
            statesController = StatesController.Instance;
            modesController = ModesController.Instance;
			modesController.OnAppModeChange += OnModeChange;

            foreach (var button in SimulateButtons)
            {
                button.onClick.AddListener(delegate { OnButtonPress(button.GetComponent<SimulateStateData>().state); });
            }
            currentMode.text = modesController.GetCurrentMode().ToString();
            currentState.text = statesController.GetCurrentState().ToString();

			useServerData.onValueChanged.AddListener(delegate { appManager.controllAppFromServer = useServerData.isOn; });
        }

        public void OnButtonPress(SimulateStates state)
        {
            switch (state)
            {
                case SimulateStates.PreStandby:
                    modesController.SetMode(AppModes.PreStandby, true);
                    currentMode.text = AppModes.PreStandby.ToString();
                    break;
                case SimulateStates.Standby:
                    modesController.SetMode(AppModes.Standby, true);
                    currentMode.text = AppModes.Standby.ToString();
                    break;
                case SimulateStates.ForceStandby:
                    modesController.SetMode(AppModes.ForceStandby, true);
                    currentMode.text = AppModes.ForceStandby.ToString();
                    break;
                case SimulateStates.Interaction:
                    modesController.SetMode(AppModes.Interaction, true);
                    currentMode.text = AppModes.Interaction.ToString();
                    break;
				case SimulateStates.SeatVideos:
                    modesController.SetMode(AppModes.SeatVideos, true);
                    currentMode.text = AppModes.SeatVideos.ToString();
                    break;
            }
        }

		void OnModeChange(AppModes mode)
		{
			currentMode.text = mode.ToString();
		}
    }

}

