﻿using CasaSeat;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RangeSlider : MonoBehaviour
{
    public bool isRange = true;
    public float minValue = 0;
    public float maxValue = 1;

    public Slider slider1;
    public Slider slider2;
    public CHARACTER_MODIFIER sliderType; 
    public Action<RangeSlider> OnChanged;

    bool isCallback = true;
    bool isInited = false;
    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!isInited) Init();
        slider1.onValueChanged.AddListener(OnValueChanged);
        slider2.onValueChanged.AddListener(OnValueChanged);
        if (!isRange) slider2.gameObject.SetActive(false);
    }

    void Init()
    {
        slider1.minValue = minValue;
        slider1.maxValue = maxValue;

        slider2.minValue = minValue;
        slider2.maxValue = maxValue;

        isInited = true;
    }

    private void OnValueChanged(float arg0)
    {
        if (isCallback && OnChanged != null) OnChanged(this);
    }

    public void SetValues(float val1, float val2 = 0)
    {
        if (!isInited) Init();

        isCallback = false;
        slider1.value = val1;
        slider2.value = val2;
        isCallback = true;
    }

    public float GetValue()
    {
        float _val;
        if (isRange)
        {
            _val = UnityEngine.Random.Range(slider1.value, slider2.value);
        } else
        {
            _val = slider1.value;
        }

        return _val;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
