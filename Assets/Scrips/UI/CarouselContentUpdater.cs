﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

namespace CasaSeat
{
	public class CarouselContentUpdater : MonoBehaviour
	{
		public GraphicOverlays stateType;
		public OverlayName overlayName;

		private ServerDataController serverDataController;
		private SlideContentReference[] contentReferences;

		private string[] splitString = {"|\n"};

		void Start()
		{
            serverDataController = ServerDataController.Instance;
		}

		public void SetContentReferences()
		{
			contentReferences = GetComponentsInChildren<SlideContentReference>(true) as SlideContentReference[];
		}

		public void UpdateContent()
		{
            //Debug.Log("UpdateContent: " + stateType + " // " + overlayName);

            DomesticApiData data = ServerDataController.overlayData;
            if (!ServerDataController.hasData) return;

            if (stateType == GraphicOverlays.Mobility)
            {
                switch (overlayName)
                {
                    case OverlayName.overlay_1:
                        UpdateContent(data.available_copies.Mobility.overlay_1);
                        return;
                    case OverlayName.overlay_2:
                        //UpdateContent(data.available_copies.Mobility.overlay_2);
                        return;
                    case OverlayName.overlay_3:
                        UpdateContent(data.available_copies.Mobility.overlay_3);
                        return;
                }
            }

            else if (stateType == GraphicOverlays.Clima)
            {
                switch (overlayName)
                {
                    case OverlayName.overlay_4:
                        UpdateContent(data.available_copies.Clima.overlay_4);
                        return;
                }
            }

            else if (stateType == GraphicOverlays.Events)
            {
                switch (overlayName)
                {
                    case OverlayName.overlay_5:
                        UpdateContent(data.available_copies.Events.overlay_5);
                        return;
                    case OverlayName.overlay_6:
                        UpdateContent(data.available_copies.Events.overlay_6);
                        return;
                    case OverlayName.overlay_3:
                        UpdateContent(data.available_copies.Events.overlay_3);
                        return;
                    case OverlayName.overlay_7:
                        //UpdateContent(data.available_copies.Events.overlay_7);
                        return;
                    case OverlayName.overlay_8:
                        UpdateContent(data.available_copies.Events.overlay_8);
                        return;
                }
            }
        }

        void UpdateContent(CopyData[] options)
		{
			CopyData option = options[UnityEngine.Random.Range(0, options.Length)];

			string[] dataTexts = option.data.Split(splitString, StringSplitOptions.None);

            /*
			Debug.Log("---------------");
			Debug.Log(option.data);
			Debug.Log(stateType);
			Debug.Log(overlayName);
			Debug.Log("server texts : " + dataTexts.Length + " text fields : " + (contentReferences[0].counterTexts.Length + contentReferences[0].staticTexts.Length));
            */

			if(dataTexts.Length != (contentReferences[0].counterTexts.Length + contentReferences[0].staticTexts.Length)) 
				throw new System.Exception("Texts dont match template");

			foreach (SlideContentReference slideContent in contentReferences)
			{
				if(slideContent.mainStaticText.gameObject.activeInHierarchy) slideContent.mainStaticText.text = option.main;
				
				int counterIndex = 0;
				int normalIndex = 0;

                for (int i = 0; i < dataTexts.Length; i++)
                {
                    string text = dataTexts[i];
                    if (Regex.Match(text, @"^[0-9,.-]*$").Success)
                    {
                        CounterText counterItem = slideContent.counterTexts[counterIndex];
                        counterItem.text.text = text;
                        counterItem.startValue = 0;
                        counterItem.endValue = int.Parse(text.Replace(",", ""));
                        counterIndex++;
                    }
                    else
                    {
                        slideContent.staticTexts[normalIndex].text = text;
                        normalIndex++;
                    }
                }
                /*
				foreach (string text in dataTexts)
				{
					if(Regex.Match(text, @"^(\d*\.?\d+|\d{1,3}(,\d{3})*(\.\d+)?)$").Success) {
						CounterText counterItem = slideContent.counterTexts[counterIndex];
						counterItem.text.text = text;
						counterItem.startValue = 0;
						counterItem.endValue = int.Parse(text.Replace(",", ""));
						counterIndex++;
					} else {
						slideContent.staticTexts[normalIndex].text = text; 
						normalIndex++; 
					}
				}
                */
			}
		}
	}

	public enum OverlayName
	{
		overlay_1,
		overlay_2,
		overlay_3,
		overlay_4,
		overlay_5,
		overlay_6,
		overlay_7,
		overlay_8
	}
}
