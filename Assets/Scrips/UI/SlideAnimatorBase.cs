﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
	public class SlideAnimatorBase : MonoBehaviour
	{
		public float contentItemsAnimateTime;
		public float contentItemAnimateDelay;
		public LeanTweenType contentEasingType;
		public MoveAndText[] animateItems;
		public GameObject[] hideOnFirstSlide;

        private int screenHeight = 1000;

		public void AnimateContentsIn()
		{
			for (int i = 0; i < animateItems.Length; i++)
			{
				MoveAndText animateItem = animateItems[i];
				RectTransform item = animateItem.moveContainer;

				LeanTween.cancel(item);
				LeanTween.move(item, Vector2.zero, contentItemsAnimateTime).setEase(contentEasingType).setDelay((i+1) * contentItemAnimateDelay + 0.5f).setOnStart(() => {
				
					if(animateItem.textAnimations.Length > 0)
					{
						foreach (TextItemBase animation in animateItem.textAnimations)
						{
							animation.AnimateIn();
						}
					}
				});
			}
		}

		public void ResetAnimations()
		{
            for (int i = 0; i < animateItems.Length; i++)
			{
				RectTransform item = animateItems[i].moveContainer;
                LeanTween.cancel(item);
                Vector2 pos = new Vector2(0, -screenHeight);
				item.anchoredPosition = pos;

				if(animateItems[i].textAnimations.Length > 0)
				{
					foreach (TextItemBase animation in animateItems[i].textAnimations)
					{
						animation.ResetAnimation();
					}
				}
			}
		}

		public void HideItemsFirstSlide()
		{
			if(hideOnFirstSlide.Length > 0)
			{
				foreach (GameObject item in hideOnFirstSlide)
				{
					item.SetActive(false);
				}
			}
		}
	}

	[System.Serializable]
	public class MoveAndText
	{
		public RectTransform moveContainer;
		public TextItemBase[] textAnimations;
	}	
}