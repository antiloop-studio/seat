﻿using CasaSeat;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideBase : MonoBehaviour
{
    public GraphicOverlays stateType;
    public OverlayName overlayName;

    public float duration = 10;
    public float textBottomDelay = 5;
    public bool isAnimInTwoPhases;
    public bool isAnimTwoSlides;
    public bool isFirstLineSecondPhase = true;
    public bool isDelayClimateMode = false;
    public float firstPhaseDisplacement = 450;
    public float secondPhaseDelay = 4;

    public Text[] textsTop;
    internal List<float> textsTopPosition = new List<float>();
    public Text textBottom;
    internal float textBottomPosition;
    private ServerDataController serverDataController;



    private GameObject container;
    private string[] splitString = { "|\n" };
    private float lineAnimDelay = 0.2f;
    // Start is called before the first frame update
    public virtual void Start()
    {
        container = textBottom.transform.parent.gameObject;
        serverDataController = ServerDataController.Instance;
        
        for (int i = 0; i < textsTop.Length; i++)
        {
            float pos = textsTop[i].rectTransform.anchoredPosition.y;
            textsTopPosition.Add(pos);
        }
        textBottomPosition = textBottom.rectTransform.anchoredPosition.y;

        ResetSlide();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Show()
    {
        //container.SetActive(true);
        UpdateContent();

        if (isAnimInTwoPhases) { 
            for (int i = 0; i < textsTop.Length; i++)
            {
                float _t = textsTopPosition[i] - firstPhaseDisplacement;
                if (i == 0 && !isFirstLineSecondPhase) _t = textsTopPosition[i];

                LeanTween.moveY(textsTop[i].rectTransform, _t, 2).setEase(LeanTweenType.easeOutExpo).setDelay(i * lineAnimDelay);
            }

            LeanTween.delayedCall(gameObject, secondPhaseDelay, () => {
                for (int i = 0; i < textsTop.Length; i++)
                {
                    LeanTween.moveY(textsTop[i].rectTransform, textsTopPosition[i], 2).setEase(LeanTweenType.easeInOutExpo).setDelay(i * lineAnimDelay);
                }
            });
        } else
        {
            LeanTween.delayedCall(gameObject, 0, () => {
                float _d = 0;
                for (int i = 0; i < textsTop.Length; i++)
                {
                    LeanTween.moveY(textsTop[i].rectTransform, textsTopPosition[i], 2).setEase(LeanTweenType.easeOutExpo).setDelay(_d);
                    _d += lineAnimDelay;
                    if (isDelayClimateMode && (float)i % 2f != 0) _d += 0.6f;
                }
            });
        }

        float _delaySecondSlide = 0;
        if (isAnimTwoSlides)
        {
            for (int i = 0; i < textsTop.Length; i++)
            {
                LeanTween.moveY(textsTop[i].rectTransform, 600, 2).setEase(LeanTweenType.easeInExpo).setDelay(i * lineAnimDelay + textBottomDelay);
            }
            _delaySecondSlide += textsTop.Length * lineAnimDelay + 1.5f;
        }
        LeanTween.moveY(textBottom.rectTransform, textBottomPosition, 2).setEase(LeanTweenType.easeOutExpo).setDelay(textBottomDelay + _delaySecondSlide);

        LeanTween.delayedCall(gameObject, duration, () => {
            for (int i = 0; i < textsTop.Length; i++)
            {
                LeanTween.moveY(textsTop[i].rectTransform, 600, 2).setEase(LeanTweenType.easeInExpo).setDelay(i * lineAnimDelay);
            }
            LeanTween.moveY(textBottom.rectTransform, 1024f, 2).setEase(LeanTweenType.easeInExpo).setDelay((textsTop.Length + 1) * lineAnimDelay);
        });
    }

    public virtual void ResetSlide()
    {
        LeanTween.cancel(gameObject);
        for (int i = 0; i < textsTop.Length; i++)
        {
            LeanTween.cancel(textsTop[i].rectTransform);
            textsTop[i].rectTransform.anchoredPosition = new Vector2(textsTop[i].rectTransform.anchoredPosition.x, -1024);
        }
        LeanTween.cancel(textBottom.rectTransform);
        textBottom.rectTransform.anchoredPosition = new Vector2(textBottom.rectTransform.anchoredPosition.x, -1024);

        //container.SetActive(false);
    }

    public void UpdateContent()
    {
        if (!ServerDataController.hasData)
        {
            textBottom.text = "";
            for (int i = 0; i < textsTop.Length; i++)
            {
                Text _t = textsTop[i];
                _t.text = "";
            }
            return;
        }

        CopyData[] _data = GetData();
        CopyData option = _data[UnityEngine.Random.Range(0, _data.Length)];

        textBottom.text = option.main;

        string[] dataTexts = option.data.Split(splitString, StringSplitOptions.None);

        if (dataTexts.Length != textsTop.Length)
        {
            Debug.Log("UpdateContent: " + stateType + " // " + overlayName);
            throw new System.Exception("Texts dont match template");
        }

        for (int i = 0; i < textsTop.Length; i++)
        {
            Text _t = textsTop[i];
            _t.text = dataTexts[i];
        }

    }

    private CopyData[] GetData()
    {
        DomesticApiData data = ServerDataController.overlayData;

        switch (stateType)
        {
            case GraphicOverlays.Mobility:
                switch (overlayName)
                {
                    case OverlayName.overlay_1:
                        return data.available_copies.Mobility.overlay_1; ;
                    case OverlayName.overlay_2:
                    //UpdateContent(data.available_copies.Mobility.overlay_2);
                    case OverlayName.overlay_3:
                        return data.available_copies.Mobility.overlay_3;
                }
                break;
            case GraphicOverlays.Clima:
                switch (overlayName)
                {
                    case OverlayName.overlay_4:
                        return data.available_copies.Clima.overlay_4;
                }
                break;
            case GraphicOverlays.Events:
                switch (overlayName)
                {
                    case OverlayName.overlay_5:
                        return data.available_copies.Events.overlay_5;
                    case OverlayName.overlay_6:
                        return data.available_copies.Events.overlay_6;
                    case OverlayName.overlay_3:
                        return data.available_copies.Events.overlay_3;
                    case OverlayName.overlay_7:
                    //UpdateContent(data.available_copies.Events.overlay_7);
                    case OverlayName.overlay_8:
                        return data.available_copies.Events.overlay_8;
                }
                break;
        }

        return new CopyData[] { };

    }
}
