﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
	public class CarouselController : MonoBehaviour
	{
		public float displayTimeFirstSlide;
		public float displayTimeFinalSlide;
		public float slideAnimateSpeed;
		public float slideAnimateSpeedScroll = 3;
		public int slidesCount;
		public LeanTweenType slideEasing;
		public LeanTweenType slideEasingOut;
		public SlideAnimatorBase mainSlideContainer;

		private RectTransform slidesContainer;
		private CarouselContentUpdater contentUpdater;
        private int screenHeight = 1000;
        private List<SlideAnimatorBase> slides = new List<SlideAnimatorBase>();
        void Start()
		{
			contentUpdater = this.GetComponent<CarouselContentUpdater>();
			slidesContainer = this.GetComponent<RectTransform>();

            slides.Add(mainSlideContainer);

            for (int i = 1; i < slidesCount; i++)
			{
				GameObject go = Instantiate(mainSlideContainer.gameObject, transform);
				go.GetComponent<RectTransform>().anchoredPosition = new Vector2(60, -60 - screenHeight * i);
                slides.Add(go.GetComponent<SlideAnimatorBase>());
            }

			contentUpdater.SetContentReferences();
			ResetCarousel();
		}

		void Update()
		{
			// REMOVE
			//if(Input.GetKeyDown(KeyCode.A)) AnimateIn();
		}

		public void AnimateIn()
		{
			ResetCarousel();
            contentUpdater.UpdateContent();

            mainSlideContainer.gameObject.SetActive(true);
			mainSlideContainer.AnimateContentsIn();
            LeanTween.move(slidesContainer, new Vector2(0, 0), slideAnimateSpeed).setEase(slideEasing);

            StartCoroutine(DelayBeforeAnimation(displayTimeFirstSlide, AnimateToFinalSlide));
		}

		void AnimateToFinalSlide()
		{
            OSCController.SendMessage(OSCController.OSC_MSG.TEXT_SCROLL);
            LeanTween.move(slidesContainer, new Vector2(0, screenHeight * 1), slideAnimateSpeedScroll).setEase(slideEasing);
            if (slidesCount > 1) StartCoroutine(DelayBeforeAnimation(displayTimeFinalSlide, AnimateOut));
			else ResetCarousel();
		}

		IEnumerator DelayBeforeAnimation(float delay, Action animateAction)
		{
			yield return new WaitForSeconds(delay);
			animateAction();
		}

		void AnimateOut() 
		{
            LeanTween.move(slidesContainer, new Vector2(0, screenHeight * slidesCount), slideAnimateSpeed).setEase(slideEasingOut);
        }
 
		public void ResetCarousel()
		{
            StopAllCoroutines();
            LeanTween.cancel(slidesContainer);

            mainSlideContainer.HideItemsFirstSlide();
            mainSlideContainer.ResetAnimations();
            foreach (var item in slides)
            {
            }

			slidesContainer.anchoredPosition = new Vector2(0, -screenHeight);

			// mainSlideContainer.gameObject.SetActive(false);
		}


	}

}

