using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
	public class TextItemBase : MonoBehaviour
	{
		public float animateTime;

		public virtual void Start() { }

		public virtual void Update() { }

		public virtual void Draw() { }

		public virtual void AnimateIn() { }

		public virtual void ResetAnimation() { }

		public virtual void ShowFinalText() { }
	}
}