﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
	public class CounterText : TextItemBase
	{
		public int startValue, endValue;
		public LeanTweenType easingType;
		public Text text;

		public override void Start()
		{
			ShowFinalText();
		}

		public override void AnimateIn()
		{
			ResetAnimation();
            OSCController.SendMessage(OSCController.OSC_MSG.NUM_COUNTER);
			LeanTween.value(gameObject, startValue, endValue, animateTime).setOnUpdate((float val) => {
				text.text = String.Format("{0:n0}", (int)val);
			}).setEase(easingType);
		}

		public override void ShowFinalText()
		{
			LeanTween.cancel(gameObject);
			text.text = String.Format("{0:n0}", endValue);
		}

		public override void ResetAnimation()
		{
			LeanTween.cancel(gameObject);
			text.text = String.Format("{0:n0}", startValue);
		}
	}
	
}