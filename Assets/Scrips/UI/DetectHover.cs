﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DetectHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Action OnOverEvent;
    public Action OnOutEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (OnOverEvent != null) OnOverEvent.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (OnOutEvent != null) OnOutEvent.Invoke();
    }
}
