﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISlideBase : MonoBehaviour
{
    [Header("GENERIC")]
    public bool debug;
    [HideInInspector] public bool isSlideIn = false;
    public RectTransform mainSlideContainer;
    public GameObject[] itemsToHideFirstSlide;
    public RectTransform[] slideItemsToAnimIn;
    public float slideRollDelay;
    public float slideItemsInDelay;

    protected GameObject container;
    protected RectTransform rt;
    protected float slideRollTime = 2.5f;
    protected float slideItemsInTime = 1f;
    protected List<RectTransform> slideContainers = new List<RectTransform>();

    
    // Start is called before the first frame update
    public virtual void Start()
    {
        rt = GetComponent<RectTransform>();

        //Duplicates the slide
        slideContainers.Add(mainSlideContainer);
        for (int i = 0; i < 2; i++)
        {
            GameObject go = Instantiate(mainSlideContainer.gameObject, mainSlideContainer.transform.parent);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.anchorMin = new Vector2(0, 0.333f * i);
            rt.anchorMax = new Vector2(0, 0.333f * i + 0.333f);
            slideContainers.Add(rt);
        }

        //Hides items on first slide
        for (int i = 0; i < itemsToHideFirstSlide.Length; i++)
        {
            GameObject item = itemsToHideFirstSlide[i];
            item.SetActive(false);
        }

        //Resets positions
        SlideReset();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (debug) { 
            if (Input.GetKeyDown(KeyCode.A)) SlideIn();
            if (Input.GetKeyDown(KeyCode.S)) SlideOut();
        }
    }

    public virtual void SlideIn()
    {
        
        isSlideIn = true;
        float screenH = Screen.height;
        Vector2 pos = new Vector2(0, 2 * Screen.height);
        LeanTween.cancel(rt);
        LeanTween.move(rt, pos, slideRollTime).setEase(LeanTweenType.easeInOutExpo).setDelay(slideRollDelay);

        for (int i = 0; i < slideItemsToAnimIn.Length; i++)
        {
            RectTransform item = slideItemsToAnimIn[i];
            LeanTween.cancel(item);
            LeanTween.move(item, Vector2.zero, slideItemsInTime).setEase(LeanTweenType.easeOutQuad).setDelay(i * slideItemsInDelay);
        }
    }

    public virtual void SlideOut()
    {
        isSlideIn = false;

        Vector2 pos = new Vector2(0, 3 * Screen.height);
        LeanTween.move(rt, pos, 1).setEase(LeanTweenType.easeInQuad).setOnComplete(SlideReset);
        
    }

    public virtual void SlideReset()
    {
        LeanTween.cancel(rt);
        rt.anchoredPosition = Vector2.zero;
        //Sets default position for masked items
        for (int i = 0; i < slideItemsToAnimIn.Length; i++)
        {
            RectTransform item = slideItemsToAnimIn[i];
            LeanTween.cancel(item);
            Vector2 pos = new Vector2(0, -Screen.height);
            item.anchoredPosition = pos;
        }
    }
}
