﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISlideEvents : UISlideBase
{
    [Header("SLIDE TYPE SPECIFIC")]
    public int startNumber;
    public int targetNumber;
    public Text textCounter;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void SlideIn()
    {
        base.SlideIn();

        LeanTween.value(gameObject, updateValueExampleCallback, startNumber, targetNumber, 1.5f).setDelay(slideItemsInDelay).setEase(LeanTweenType.easeInOutSine);
    }

    void updateValueExampleCallback(float val, float ratio)
    {
        textCounter.text = (int)val + "";
    }

    public override void SlideOut()
    {
        base.SlideOut();
    }

    public override void SlideReset()
    {
        Debug.Log("RESET SLIDE");
        base.SlideReset();
        textCounter.text = startNumber + "";
    }
}
