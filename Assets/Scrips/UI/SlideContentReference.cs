﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CasaSeat
{
	public class SlideContentReference : MonoBehaviour
	{
		public Text mainStaticText;
		public Text[] staticTexts;
		public CounterText[] counterTexts;
	}
}