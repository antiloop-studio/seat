﻿using UnityEngine;

namespace CasaSeat
{
    public class CharacterAppearanceData
    {
        public float[][] originalValues;
        public float[] processedValues;

        public bool particleHasCollider;
        public bool particleHasPhysics;

        public bool hasTopRing;
        public bool hasBottomRing;

        public CharacterAppearanceData()
        {
            particleHasCollider = false;
            particleHasPhysics = false;

            hasTopRing = false;
            hasBottomRing = false;

            originalValues = new float[][]
            {
                new float[] { 0.05f, 1}, //HEAD
                new float[] { 1, 1}, // NECK
                new float[] { 1, 1}, // SHOULDER X
                new float[] { 1, 1}, // SHOULDER Y
                new float[] { 1, 1}, // UPPER ARMS
                new float[] { 1, 1}, // LOWER ARMS
                new float[] { 1, 1}, // TORSO
                new float[] { 1, 1}, // WAIST WIDTH
                new float[] { 1, 1}, // WAIST HEIGHT
                new float[] { 1, 1}, // HIPS
                new float[] { 1, 1}, // UPPER LEGS
                new float[] { 1, 1}, // LOWER LEGS

                new float[] { 0.05f, 0.05f}, // HEAD RADIUS
                new float[] { 0.02f, 0.02f}, // NECK TOP R
                new float[] { 0.02f, 0.02f}, // SHOULDER SPHERE R
                new float[] { 0.02f, 0.02f}, // ARMS TOP R
                new float[] { 0.02f, 0.02f}, // ARMS MIDDLE R
                new float[] { 0.02f, 0.02f}, // NECK BOTTOM R
                new float[] { 0.02f, 0.02f}, // ARMS BOTTOM R
                new float[] { 1, 1}, // TORSO UP
                new float[] { 1, 1}, // TORSO MIDDLE
                new float[] { 1, 1}, // TORSO DOWN
                new float[] { 0.05f, 0.05f}, // HIP SPHERE
                new float[] { 0.02f, 0.02f}, // LEGS TOP
                new float[] { 0.02f, 0.02f}, // LEGS MID
                new float[] { 0.02f, 0.02f}, // LEGS BOTTOM

                new float[] { 0.05f, 0.1f}, // PARTICLE SIZE
                new float[] { 0.1f, 0.2f}, // PARTICLE SPACING
                new float[] { 0.1f, 0.1f}, // PARTICLE OFFSET
                new float[] { 100, 100}, // PARTICLE BASE FORCE
                new float[] { 1, 1}, // PARTICLE FORCE MULT
                new float[] { 1, 1}, // PARTICLE DIST MULT
                new float[] { 0.1f, 0.1f}, // PARTICLE HEADING EASE

                new float[] { 1, 1}, // PARTICLE TYPE 1
                new float[] { 0, 0}, // PARTICLE TYPE 2
                new float[] { 0, 0}, // PARTICLE TYPE 3
                new float[] { 0, 0}, // PARTICLE TYPE 4
                new float[] { 0, 0}, // PARTICLE TYPE 5
                new float[] { 0, 0}, // PARTICLE TYPE 6
                new float[] { 0, 0}, // PARTICLE TYPE 7
                new float[] { 0, 0}, // PARTICLE TYPE 8
                new float[] { 0, 0}, // PARTICLE TYPE 9
                new float[] { 0, 0}, // PARTICLE TYPE 10
                new float[] { 0, 0}, // PARTICLE TYPE 11
                new float[] { 0, 0}, // PARTICLE TYPE 12
                new float[] { 0, 0}, // PARTICLE TYPE 13
                new float[] { 0, 0}, // PARTICLE TYPE 14
                new float[] { 0, 0}, // PARTICLE TYPE 15
                new float[] { 0, 0}, // PARTICLE TYPE 16
                new float[] { 0, 0}, // PARTICLE TYPE 17
                new float[] { 0, 0}, // PARTICLE TYPE 18
                new float[] { 0, 0}, // PARTICLE TYPE 19
                new float[] { 0, 0}, // PARTICLE TYPE 20
                new float[] { 0, 0}, // PARTICLE TYPE 21

                new float[] { 0.7f, 0.7f}, // NECK RING TOP HEIGHT
                new float[] { 0.05f, 0.05f}, // NECK RING TOP RADIUS
                new float[] { 0.3f, 0.3f}, // NECK RING TOP CURVE RADIUS

                new float[] { 0.4f, 0.4f}, // NECK RING BOTTOM HEIGHT
                new float[] { 0.02f, 0.02f}, // NECK RING BOTTOM RADIUS
                new float[] { 0.15f, 0.15f}, // NECK RING BOTTOM CURVE RADIUS
            };
            processedValues = new float[originalValues.Length];
            ProcessValues();
        }

        public void ProcessValues()
        {
            for (int i = 0; i < originalValues.Length; i++)
            {
                processedValues[i] = Random.Range(originalValues[i][0], originalValues[i][1]);
            }
        }

        public static CharacterAppearanceData Lerp(CharacterAppearanceData a, CharacterAppearanceData b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            CharacterAppearanceData result = new CharacterAppearanceData();

            for (int i = 0; i < result.originalValues.Length; i++)
            {
                if (i < (int)CHARACTER_MODIFIER.PARTICLE_TYPE_1 || i > (int)CHARACTER_MODIFIER.PARTICLE_TYPE_21)
                    result.processedValues[i] = Mathf.Lerp(a.processedValues[i], b.processedValues[i], t);
                    result.originalValues[i][0] = Mathf.Lerp(a.originalValues[i][0], b.originalValues[i][0], t);
                    result.originalValues[i][1] = Mathf.Lerp(a.originalValues[i][1], b.originalValues[i][1], t);
            }

            return result;
        }
    } 
}

