﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITab : MonoBehaviour
{
    public GameObject[] tabContent;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < tabContent.Length; i++)
        {
            tabContent[i].SetActive(false);
        }
        
    }

    public void SetTab(int id) {
        for (int i = 0; i < tabContent.Length; i++)
        {
            tabContent[i].SetActive(i == id);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
