﻿using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

public class Torus : MonoBehaviour {

    // Use this for initialization
    public bool debug;
    public bool createCollider = true;
    public int curveSegmentCount, torusSegmentCount;

   

    [SerializeField]
    private float _torusColliderSegmentCount;
    public float TorusColliderSegmentCount
    {

        get { return _torusColliderSegmentCount; }
        set
        {
            _torusColliderSegmentCount = value;
            CreateTorus();
        }
    }

    [SerializeField]
    private float _curveColliderSegmentCount;
    public float CurveColliderSegmentCount
    {

        get { return _curveColliderSegmentCount; }
        set
        {
            _curveColliderSegmentCount = value;
            CreateTorus();
        }
    }

    [SerializeField]
    private float _curveRadius;
    public float CurveRadius
    {

        get { return _curveRadius; }
        set
        {
            _curveRadius = value;
            CreateTorus();
        }
    }

    [SerializeField]
    private float _torusRadius;
    public float TorusRadius
    {
        get { return _torusRadius; }
        set
        {
            _torusRadius = value;
            CreateTorus();
        }
    }

    //Mesh Render
    private Mesh mesh;
    private Mesh meshCollider;
    private MeshCollider collider;
    private GameObject colliderContainer;
    

    private void Awake()
    {
        
    }

    void Start() {
        CreateTorus();
    }

    void CreateTorus() {
        if (CurveRadius == 0 || TorusRadius == 0)
        {
            GetComponent<MeshFilter>().mesh = null;
            mesh.Clear();
            return;
        }

        transform.eulerAngles = new Vector3(-90, 0, 0);
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Torus";
        SetVertices(mesh, torusSegmentCount, curveSegmentCount);
        SetTriangles(mesh, torusSegmentCount, curveSegmentCount);
        mesh.RecalculateNormals();

        if (createCollider) {
            
            if (colliderContainer) Destroy(colliderContainer);

            colliderContainer = CreateRing(PivotAxis.Y, 10, CurveRadius + TorusRadius, CurveRadius - TorusRadius, ColliderShape.Capsule, TorusRadius, 0, false);
            colliderContainer.transform.parent = transform;
            colliderContainer.transform.localPosition = Vector3.zero;

            colliderContainer.layer = gameObject.layer;

            foreach (Transform child in colliderContainer.transform)
            {
                child.gameObject.layer = gameObject.layer;
            }
        }
        
    }

    //Create Torus
    private Vector3 GetPointOnTorus(float u, float v)
    {
        Vector3 p;
        float r = (_curveRadius + _torusRadius * Mathf.Cos(v));
        p.x = r * Mathf.Sin(u);
        p.y = r * Mathf.Cos(u);
        p.z = _torusRadius * Mathf.Sin(v);
        return p;
    }

    //Draw Torus
    private void OnDrawGizmos()
    {
        if (!debug) return;

        float uStep = (2f * Mathf.PI) / curveSegmentCount;
        float vStep = (2f * Mathf.PI) / torusSegmentCount;

        for (int u = 0; u < curveSegmentCount; u++)
        {
            for (int v = 0; v < torusSegmentCount; v++)
            {
                Vector3 point = GetPointOnTorus(u * uStep, v * vStep);
                Gizmos.color = new Color(1f, (float)v / torusSegmentCount,(float)u / curveSegmentCount);
                Gizmos.DrawSphere(point, 0.1f);
            }
        }
    }

    //each quad share vertices with its neighbors, or give each quad its own four vertices.
    private void SetVertices(Mesh _m, int _torusSegment, int _curveSegment)
    {
        Vector3[] vertices = new Vector3[_torusSegment * _curveSegment * 4];
        float uStep = (2f * Mathf.PI) / _curveSegment;
        vertices = CreateFirstQuadRing(vertices, uStep, _torusSegment, _curveSegment);
        int iDelta = _torusSegment * 4;
        for (int u = 2, i = iDelta; u <= _curveSegment; u++, i += iDelta)
        {
            vertices = CreateQuadRing(vertices, u * uStep, i, _torusSegment, _curveSegment);
        }
        _m.vertices = vertices;
    }

    //Each quad has two triangles, so six vertex indices.
    private void SetTriangles(Mesh _m, int _torusSegment, int _curveSegment)
    {
        int[] triangles = new int[_torusSegment * _curveSegment * 6];
        for (int t = 0, i = 0; t < triangles.Length; t += 6, i += 4)
        {
            triangles[t] = i;
            triangles[t + 1] = triangles[t + 4] = i + 1;
            triangles[t + 2] = triangles[t + 3] = i + 2;
            triangles[t + 5] = i + 3;
        }
        _m.triangles = triangles;
    }

    private Vector3[] CreateFirstQuadRing(Vector3[] _vertices, float u, int _torusSegment, int _curveSegment)
    {
        float vStep = (2f * Mathf.PI) / _torusSegment;

        Vector3 vertexA = GetPointOnTorus(0f, 0f);
        Vector3 vertexB = GetPointOnTorus(u, 0f);
        for (int v = 1, i = 0; v <= _torusSegment; v++, i += 4)
        {
            _vertices[i] = vertexA;
            _vertices[i + 1] = vertexA = GetPointOnTorus(0f, v * vStep);
            _vertices[i + 2] = vertexB;
            _vertices[i + 3] = vertexB = GetPointOnTorus(u, v * vStep);
        }
        return _vertices;
    }

    private Vector3[] CreateQuadRing(Vector3[] _vertices, float u, int i, int _torusSegment, int _curveSegment)
    {
        float vStep = (2f * Mathf.PI) / _torusSegment;
        int ringOffset = _torusSegment * 4;

        Vector3 vertex = GetPointOnTorus(u, 0f);
        for (int v = 1; v <= _torusSegment; v++, i += 4)
        {
            _vertices[i] = _vertices[i - ringOffset + 2];
            _vertices[i + 1] = _vertices[i - ringOffset + 3];
            _vertices[i + 2] = vertex;
            _vertices[i + 3] = vertex = GetPointOnTorus(u, v * vStep);
        }
        return _vertices;
    }

    
    public enum PivotAxis
	{
		X,
		Y,
		Z
	};
    public enum ColliderShape
	{
		Box,
		Capsule
	};
    /// <summary>
		/// Creates a ring-shaped compound collider game object.
		/// </summary>
		public static GameObject CreateRing(PivotAxis pivotAxis, int sides, float outerRadius, float innerRadius,
			ColliderShape colliderShape, float height, float rotationOffset, bool hasEndCap)
		{
			// Empty game object that will serve as the root of the compound collider "prefab"
			var compoundCollider = new GameObject("Ring Compound Collider");

			float length = 2*outerRadius*Mathf.Tan(Mathf.PI/sides); // Length follows the flow of the ring of the torus
			float width = outerRadius - innerRadius;

			if (colliderShape.Equals(ColliderShape.Capsule))
			{
				// Capsule length needs to be adjusted to factor in spherical caps
				length = (length / outerRadius * ((outerRadius + innerRadius) / 2)) + (width);
			}

			for (int i = 0; i < sides; i++)
			{
				// Create the new collider object
				GameObject segment = null;

				if (colliderShape.Equals(ColliderShape.Box))
				{
					segment = GameObject.CreatePrimitive(PrimitiveType.Cube);
					segment.transform.localScale = new Vector3(length, height, width); // Set scale
					segment.transform.Rotate(Vector3.right, rotationOffset); // Apply lengthwise rotation

					// Set the renderer material on the segment object
					//segment.GetComponent<Renderer>().material = colliderMaterial;
				}
				else if (colliderShape.Equals(ColliderShape.Capsule))
				{
					// Create segment game object and set its position
					segment = new GameObject("Capsule");

					// Add CapsuleCollider component
					var capsuleCollider = segment.AddComponent<CapsuleCollider>();

					// Turn capsule so it lies length-wise along the X-axis
					capsuleCollider.direction = 0;

					capsuleCollider.height = length;
					capsuleCollider.radius = width / 2;
				}				

				float segmentAngleDegrees = i*(360f/sides);

				// Position the segment relative to its parent
				segment.transform.position = new Vector3(0f, 0f, outerRadius - (width/2));
				segment.transform.RotateAround(Vector3.zero, Vector3.up, segmentAngleDegrees);

				// Rotate to matcb pivot axis
				if (pivotAxis != PivotAxis.Y) // We do Y by default so skip this step if Y
				{
					Vector3 rotationAxis = (pivotAxis == PivotAxis.Z) ? Vector3.right : Vector3.forward;
					segment.transform.RotateAround(Vector3.zero, rotationAxis, 90f);
				}

				// Set the segment parent to the compound collider GameObject
				segment.transform.SetParent(compoundCollider.transform, true);
			}

			// Add the cap if enabled
			if (hasEndCap)
			{
				var capObject = CreateCylinderPrimitive(sides, outerRadius, width);

				// Position the cap on the bottom of the tube
				capObject.transform.position = new Vector3(0f, -((height/2) + width/2), 0f);

				// Set the cap's parent to the compound collider GameObject
				capObject.transform.SetParent(compoundCollider.transform, true);

				// Set the renderer material on the segment object
				//capObject.GetComponent<Renderer>().material = colliderMaterial;
			}

			// Register undo event for creation of compound collider
			//Undo.RegisterCreatedObjectUndo(compoundCollider, "Create Torus Compound Collider");

			return compoundCollider;
		}
        static GameObject CreateCylinderPrimitive(int sides, float radius, float height)
		{
			var cylinder = GameObject.CreatePrimitive(PrimitiveType.Plane);

			// Generate the cylinder mesh, and assign it to the meshcollider
			var cylinderMesh = CreateCylinderMesh(sides, radius, height);
			var meshCollider = cylinder.GetComponent<MeshCollider>();
			meshCollider.sharedMesh = cylinderMesh;

			// Set mesh collider as convex so it can be used with rigidbody physics
			meshCollider.convex = true;

			// Set the cylinder mesh as the rendered mesh too
			var meshFilter = cylinder.GetComponent<MeshFilter>();
			meshFilter.mesh = cylinderMesh;

			return cylinder;
		}
        /// <summary>
		/// Returns a cylindrical mesh to fit a ring shape with given radius. Cylinder will have the given number of sides, and height.
		/// </summary>
		static Mesh CreateCylinderMesh(int sides, float radius, float height)
		{
			// circumradius = r sec(π/n)
			var circumradius = radius * 1 / Mathf.Cos(Mathf.PI / sides);

			var cylinderMesh = MeshE.Cylinder(circumradius, sides, height);

			cylinderMesh.Rotate(Quaternion.Euler(0f, (360f / sides) / 2f, 0f));

			return cylinderMesh;
		}
}