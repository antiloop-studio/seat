using System;
using UnityEngine;
using UniOSC;
using OSCsharp.Data;

namespace CasaSeat
{
	// public class OscRecieved :  UniOSCEventTarget 
	// {
	// 	public float noUserDelay, kinectSensorHeight;
	// 	public event Action<SkeletonData> OnOscSkeletonMessage;
	// 	public event Action<bool> OnSkeletonAvailability;

	// 	private SkeletonData kinectSkeleton;
	// 	private bool recievingKinectData = false;
	// 	private float timeSinceKinectMessage; 

	// 	public override void Start () {
	// 		base.Start();
	// 		kinectSkeleton = new SkeletonData();
	// 	}

	// 	public override void OnEnable(){
	// 		_Init();
	// 		base.OnEnable();
	// 	}

	// 	private void _Init(){
	// 	}

	// 	public override void OnDisable(){
	// 		base.OnDisable();
	// 	}

	// 	public override void Update () {
	// 		base.Update();
	// 		if(Time.time - timeSinceKinectMessage >= noUserDelay && recievingKinectData)
	// 		{
	// 			recievingKinectData = false;
	// 			if(OnSkeletonAvailability != null) OnSkeletonAvailability(false);
	// 		}
	// 	}

	// 	public override void OnOSCMessageReceived(UniOSCEventArgs args){
	// 		if(args.Packet.Data.Count > 0)
	// 		{
	// 			timeSinceKinectMessage = Time.time;
	// 			if(!recievingKinectData)
	// 			{
	// 				recievingKinectData = true;
	// 				if(OnSkeletonAvailability != null) OnSkeletonAvailability(true);
	// 			}

	// 			OscMessage msg = (OscMessage)args.Packet;
	// 			string jointName = msg.Address.Substring(msg.Address.LastIndexOf("/") + 1);
				
	// 			if(kinectSkeleton.joints.ContainsKey(jointName))
	// 			{
	// 				kinectSkeleton.joints[jointName] = new Vector3((float)msg.Data[0], (float)msg.Data[1], (float)msg.Data[2]); 
	// 			}

	// 			if(OnOscSkeletonMessage != null) OnOscSkeletonMessage(kinectSkeleton); 
	// 		}

	// 		// FIX: maybe replace the substring method with the method described below
	// 		//For addresses like  '/1/push8'  we could filter via these properties:
	// 		//args.Group (1)
	// 		//args.AddressRoot ("push") 
	// 		//args.AddressIndex (8)
	// 		//Debug.Log("Group: "+args.Group);
	// 		//Debug.Log("AddressRoot: "+args.AddressRoot);
	// 		//Debug.Log("AddressIndex: "+args.AddressIndex);
	// 		//if the OSC address doesn't match this pattern the Group and AddressIndex will be default -1 and AddressRoot is empty

	// 	}
	// }
}

