﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class DragMouseOrbit : MonoBehaviour
{
    public DetectHover detectHover;

    public Transform target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public Button btnFull; 

    private Rigidbody rigidbody;

    float x = 0.0f;
    float y = 0.0f;

    Vector2 originalMousePos;
    bool isAllowed = true;
    bool isValid = false;
    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        rigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (rigidbody != null)
        {
            rigidbody.freezeRotation = true;
        }

        detectHover.OnOverEvent += OnOverEvent;
        detectHover.OnOutEvent += OnOutEvent;
    }

    private void OnOutEvent()
    {
        isAllowed = true;
    }

    private void OnOverEvent()
    {
        isAllowed = false;
    }

    void LateUpdate()
    {
        transform.LookAt(target);
        if (!isAllowed) return;

        if (Input.GetMouseButtonDown(0))
        {
            originalMousePos = Input.mousePosition;
            isValid = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isValid = false;
        }


        if (target && Input.GetMouseButton(0) && isValid)
        {
            x += (originalMousePos.x - Input.mousePosition.x) * xSpeed * distance * 0.02f;
            y -= (originalMousePos.y - Input.mousePosition.y) * ySpeed * 0.02f;
            y = ClampAngle(y, yMinLimit, yMaxLimit);
        }

        Quaternion rotation = Quaternion.Euler(y, x, 0);

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

        RaycastHit hit;
        if (Physics.Linecast(target.position, transform.position, out hit))
        {
            //distance -= hit.distance;
        }
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target.position;

        transform.rotation = rotation;
        transform.position = position;

        
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}