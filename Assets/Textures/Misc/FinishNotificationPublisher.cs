﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CasaSeat
{
	public class FinishNotificationPublisher : MonoBehaviour
	{
		public float beforeFinishAmount;

		private static FinishNotificationPublisher _instance;
		public static FinishNotificationPublisher Instance
		{
			get {
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<FinishNotificationPublisher>();
					
					if (_instance == null)
					{
						GameObject container = new GameObject("FINISH NOTIFICATIONS");
						_instance = container.AddComponent<FinishNotificationPublisher>();
					}
				}
			
				return _instance;
			}
		}

		public event Action AboutToFinish, Finished;

		public void NotifyFinish(float totalDelay)
		{
			StartCoroutine(DelayBeforeFinish(totalDelay));
		}

		IEnumerator DelayBeforeFinish(float totalDelay)
		{
			float _beforeFinishAmount = beforeFinishAmount;
			if(totalDelay < _beforeFinishAmount) _beforeFinishAmount = totalDelay;

			yield return new WaitForSeconds(totalDelay - beforeFinishAmount);
			if(AboutToFinish != null) AboutToFinish();

			yield return new WaitForSeconds(beforeFinishAmount);
			if(Finished != null) Finished();
		}
	}
}