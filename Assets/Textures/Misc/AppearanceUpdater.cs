﻿namespace CasaSeat
{
    public class AppearanceUpdater
    {
        public int id;
        public string appearanceName { get; set; }
        public CharacterAppearanceData VisualData { get; set; }
        
        public AppearanceUpdater(int _id)
        {
            id = _id;
            VisualData = new CharacterAppearanceData();
            appearanceName = "appearance_" + id;
        }
    }   
}
