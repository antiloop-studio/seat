﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateWaistTransforms : MonoBehaviour
{
	public float waistWidth, waistHeight;
	public Transform spineLocation;
    public Transform left, right;
	
	void Update()
	{
		this.transform.position = spineLocation.position;
		this.transform.rotation = spineLocation.rotation;
		left.localPosition = Vector3.Scale( new Vector3(-0.1f, 0.1f, 0), new Vector3(waistWidth, waistHeight, 1));
		right.localPosition = Vector3.Scale( new Vector3(0.1f, 0.1f, 0), new Vector3(waistWidth, waistHeight, 1));
	}
}
