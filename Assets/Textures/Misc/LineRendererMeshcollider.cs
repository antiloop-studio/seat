﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererMeshcollider : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private MeshRenderer meshRenderer;
    private MeshCollider meshCollider;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        meshCollider = GetComponent<MeshCollider>();
        lineRenderer.positionCount = 200;
        for (int i = 0; i < 200; i++)
        {
            Vector3 pos;
            pos.x = Random.Range(-2f, 2f);
            pos.y = Random.Range(-2f, 2f);
            pos.z = Random.Range(-2f, 2f);

            lineRenderer.SetPosition(i, pos);
        }
        Mesh mesh = new Mesh();
        lineRenderer.BakeMesh(mesh, true);
        meshCollider.sharedMesh = mesh;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
