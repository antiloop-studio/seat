﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderValueDisplay : MonoBehaviour
{
    Text valueText;
   public Slider sliderValue;
    // Start is called before the first frame update
    void Start() {
        valueText = GetComponent<Text> ();
        

        if (sliderValue != null) {
            TextUpdate(sliderValue.value);
        }
    }

    // Update is called once per frame
    public void TextUpdate(float value) {
        if (valueText != null) {
            valueText.text = Mathf.RoundToInt(value) + "m";
        }
    }
}
