﻿using com.rfilkov.kinect;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KinectSceneManager {
    public class TopViewController : MonoBehaviour {
        public RectTransform topViewContainer;
        public Image startZoneImage;
        public Image interactionZoneImage;
        public GameObject userRef;
        public Transform userContainer;

        private float mult = 100;
        private KinectManager kinectManager;
        private ZoneController zoneController;
        private UsersController usersController;
        private Dictionary<ulong, Image> users = new Dictionary<ulong, Image>();

        private int screenX;
        private int screenY;
        // Start is called before the first frame update
        void Start() {
            kinectManager = KinectManager.Instance;
            zoneController = ZoneController.instance;
            usersController = UsersController.instance;
            zoneController.ZoneUpdated += SetupZones;

            kinectManager.userManager.OnUserAdded.AddListener(OnUserAdded);
            kinectManager.userManager.OnUserRemoved.AddListener(OnUserRemoved);

          
            OnWindowResize();
        }


        // Update is called once per frame
        void Update() {
            foreach (ulong id in users.Keys) {
                Vector3 userPos = kinectManager.GetUserPosition(id);
                users[id].rectTransform.anchoredPosition = new Vector2(userPos.x * mult, -userPos.z * mult);

                if (usersController.HasActiveUser && id == usersController.activeUser.id) {
                    users[id].color = Color.red;
                } else {
                    users[id].color = Color.blue;
                }
            }

            if (screenX != Screen.width) {
                OnWindowResize();
            }
        }

        void OnWindowResize() {
            screenX = Screen.width;
            screenY = Screen.height;
            mult = (screenX - 200) * 0.1f;
            topViewContainer.sizeDelta = new Vector2(topViewContainer.sizeDelta.x, screenX - 200f);
           
           
            SetupZones();
            //MatrixNumbering();
        }

        //create a numbering system on top of the grid 
    //    void MatrixNumbering() {
            
    //        GameObject number = new GameObject("1");
      //      number.transform.SetParent(this.transform);

       //     Text myText = number.AddComponent<Text>();
      //      myText.text = "1";
     //   }

        void SetupZones() {
            startZoneImage.rectTransform.sizeDelta = new Vector2(zoneController.startZone.width * mult, zoneController.startZone.height * mult);
            startZoneImage.rectTransform.anchoredPosition = new Vector2(0, -zoneController.startZone.y * mult);

            interactionZoneImage.rectTransform.sizeDelta = new Vector2(zoneController.interactionZone.width * mult, zoneController.interactionZone.height * mult);
            interactionZoneImage.rectTransform.anchoredPosition = new Vector2(0, -zoneController.interactionZone.y * mult);
        }

        void OnUserAdded(ulong userId, int userIndex) {
            GameObject newUser = Instantiate(userRef, userContainer);
            Image image = newUser.GetComponent<Image>();
            users.Add(userId, image);
        }

        void OnUserRemoved(ulong userId, int userIndex) {
            Destroy(users[userId].gameObject);
            users.Remove(userId);
        }
    }
}