﻿using com.rfilkov.components;
using com.rfilkov.kinect;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectSceneManager {
    
	public class KinectUser {
        public ulong id;
        public Vector2 position;
    }

    public class UsersController : MonoBehaviour {
		public static UsersController instance = null;
        public AvatarController avatarController;
        public NetClientInterface netClient;
        [HideInInspector]
        public KinectUser activeUser;
        [HideInInspector]
        public KinectUser closestUser;

        public event Action OnHasActiveUser;
        public event Action OnLostActiveUser;

        private KinectManager kinectManager;
        private KinectUserManager userManager;
        private ZoneController zoneController;
        
        private bool hasActiveUser;
        public bool HasActiveUser {
            get {
                return hasActiveUser;
            }
            private set {
                bool oldHasActiveUser = hasActiveUser;
                hasActiveUser = value;
                if (value) {
                    if (!oldHasActiveUser && OnHasActiveUser != null) OnHasActiveUser();
                } else {
                    if (oldHasActiveUser && OnLostActiveUser != null) OnLostActiveUser();
                    activeUser = null;
                    avatarController.playerIndex = -1;
                }
            }
        }

        void Awake() {
            instance = this;
            if (netClient) netClient.ConnectionErrorEvent += ConnectionErrorEvent;
        }

        private void ConnectionErrorEvent()
        {
            hasActiveUser = false;
        }

        // Start is called before the first frame update
        void Start() {
            kinectManager = KinectManager.Instance;
            userManager = kinectManager.userManager;
            zoneController = ZoneController.instance;

            userManager.OnUserRemoved.AddListener(OnUserRemoved);
            //if (avatarController) avatarController.playerIndex = -1;
        }

        // Update is called once per frame
        void Update() {
            // Tries to find the new active user
            closestUser = GetClosestUser(zoneController.startZone);

            //CHECK IF WE HAVE A LIVE CONNECTION
            if (netClient.gameObject.activeSelf && (netClient.controlFrameClient == null || !netClient.controlFrameClient.IsConnected() || !netClient.controlFrameClient.IsActive()))
            {
                if (HasActiveUser) HasActiveUser = false;
                return;
            }

            if (!HasActiveUser) {
                if (closestUser != null && zoneController.startZone.Contains(closestUser.position)) {
                    activeUser = closestUser;
                    HasActiveUser = true;
                }
            } else {
                if (!kinectManager.GetAllUserIds().Contains(activeUser.id))
                {
                    HasActiveUser = false;
                } else { 
                    Vector2 userPos = TranslateKinect2D(activeUser.id);
                    //Checks if the active user is still in the interactive zone
                    if (zoneController.interactionZone.Contains(userPos)) {
                        if (avatarController == null) {
                            Debug.Log("PLEASE ADD AN AVATAR CONTROLLLER");
                        } else {
                            int index = kinectManager.GetUserIndexById(activeUser.id);
                            if (avatarController.playerIndex != index) avatarController.playerIndex = index;
                        }
                        // Removes the active user
                    } else {
                        HasActiveUser = false;
                    }
                }
            }
        }


        // Returns the user closest to the center of the area
        KinectUser GetClosestUser(Rect area) {
            float dist = 100000000;
            KinectUser user = null;

            foreach (ulong id in userManager.alUserIds) {
                Vector2 userPos = TranslateKinect2D(id);
                float userDist = Vector2.Distance(userPos, area.center);
                if (userDist < dist) {
                    dist = userDist;
                    user = new KinectUser();
                    user.id = id;
                    user.position = userPos;
                }

            }

            return user;
        }

        // Checks if the active user has been removed
        void OnUserRemoved(ulong userId, int userIndex) {
            if (activeUser != null && activeUser.id == userId) {
                HasActiveUser = false;
            }
        }

        Vector2 TranslateKinect2D(ulong id) {
            Vector3 userPos3 = kinectManager.GetUserPosition(id);
            return new Vector2(userPos3.x, userPos3.z);
        }

    }
}