﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIVisibilityTrigger : MonoBehaviour
{
    public GameObject UI;
    bool state = false;

    void Start() {
        UI.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K)) {
            if(state == true) {
                UI.SetActive(false);
                state = false;
            } else {
                UI.SetActive(true);
                state = true;

            }
        }
    }
}
