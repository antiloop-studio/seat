﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KinectSceneManager {
    public class ZoneController : MonoBehaviour {
        public static ZoneController instance = null;

        [HideInInspector]
        public Rect startZone;
        [HideInInspector]
        public Rect interactionZone = new Rect(-5, -5, 10, 10);


        public Slider[] activationSliders;
        public Slider[] interactionSliders;

        public Action ZoneUpdated;
        // Start is called before the first frame update

        void Awake() {
            instance = this;
        }

        void Start() {
            //Start Zone Parameters 
            startZone.width = PlayerPrefs.GetFloat("StartZoneWidth");
            startZone.height = PlayerPrefs.GetFloat("StartZoneDepth");
            startZone.y = PlayerPrefs.GetFloat("StartZoneDistance");

            //Interaction Zone

            interactionZone.width = PlayerPrefs.GetFloat("InteractionZoneWidth");
            interactionZone.height = PlayerPrefs.GetFloat("InteractionZoneDepth");
            interactionZone.y = PlayerPrefs.GetFloat("InteractionZoneDistance");

            StartCoroutine(Setup());

            if (ZoneUpdated != null) ZoneUpdated();
        }

        IEnumerator Setup() {
            yield return new WaitForEndOfFrame();

            activationSliders[0].value = startZone.width;
            activationSliders[1].value = startZone.height;
            activationSliders[2].value = startZone.y;

            interactionSliders[0].value = interactionZone.width;
            interactionSliders[1].value = interactionZone.height;
            interactionSliders[2].value = interactionZone.y;
        }

        // Update is called once per frame
        void Update() {

        }

        // Start Zone
        public void SetStartZoneWidth(float val) {
            startZone.width = val;
            startZone.x = -val * 0.5f;
            PlayerPrefs.SetFloat("StartZoneWidth", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }

        public void SetStartZoneDepth(float val) {
            startZone.height = val;
            PlayerPrefs.SetFloat("StartZoneDepth", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }

        public void SetStartZoneDistance(float val) {
            startZone.y = val;
            PlayerPrefs.SetFloat("StartZoneDistance", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }

        //Interaction Zone

        public void SetInteractionZoneWidth(float val) {
            interactionZone.width = val;
            interactionZone.x = -val * 0.5f;
            PlayerPrefs.SetFloat("InteractionZoneWidth", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }

        public void SetInteractionZoneDepth(float val) {
            interactionZone.height = val;
            PlayerPrefs.SetFloat("InteractionZoneDepth", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }

        public void SetInteractionZoneDistance(float val) {
            interactionZone.y = val;
            PlayerPrefs.SetFloat("InteractionZoneDistance", val);
            if (ZoneUpdated != null) ZoneUpdated();
        }
    }
}